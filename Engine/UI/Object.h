//
//  Object.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 06/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef Object_h
#define Object_h

#include <string>
#include <memory>
#include <functional>

#include "../SDL/Surface.h"
#include "../SDL/Rectangle.h"
#include "../SDL/Point.h"

using namespace std;


class Engine;

class Object
{
    friend class Engine;
protected:
    string mName;
    shared_ptr<Surface> mspSurface;
    /*
     This is Rectangle instead of Point. Reason being
     We use everywhere elastic surface which can be zoomed out or zoomed in while painting.
     Hence irrespective of the Surface size we could paint it bigger or smaller.
     The Object class represents a visual logical object that is to be painted on
     the window. It contains a surface. However the object should be able to be moved
     and shrinked or expanded. Hence Object class contains a rectange. When the window paints
     it will take the Object's rectangle and blit.
     */
    Rectangle mRectangle;
    bool mIsActive = true;
    
    /*The window class sorts the objects based on mZIndex
     before painting them.*/
//    bool operator<(const Object object) const noexcept;
    
    //Users should not create a nameless object. Rather they should create
    //the named objects like Image, TextBox etc
    /*
     We could pass a const std::shard_ptr<Surface> &. However it does not express the shared ownership semantics clearly.
     The atomic counter is not such a huge price to pay. Atleast for now. Will think about it future.
     */
    
    Object(string name, const Rectangle &rectangle) noexcept;
    
public:
    int mZIndex = 0;
    string GetName();
    void SetRectangle(const Rectangle &rectangle) noexcept;
    Rectangle GetRectangle()const noexcept;
    bool IsActive() const noexcept;
    void SetActive(bool state) noexcept;
    void SetZIndex(int zIndex) noexcept;
    virtual ~Object();
    
    /*It is more intutive to use the event handlers this way instead of wrapping them around get/set functions*/
    std::function<void(Point)> mOnMouseButtonDown;
    std::function<void(Point)> mOnMouseButtonUp;
    std::function<void(Point)> mOnMouseOver;
    std::function<void(Point)> mOnMouseButtonClick;
    std::function<void(Point, Point)> mOnDrag;
};

#endif /* Object_h */
