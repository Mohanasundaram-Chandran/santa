//
//  Image.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 06/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef Image_h
#define Image_h

#include <string>
#include <memory>

#include "../SDL/Rectangle.h"
#include "../SDL/Colour.h"
#include "Object.h"

using namespace std;

class Image: public Object
{
public:
    Image(string name, string path, const Rectangle &rectangle);
    Image(string name, const Colour &colour, const Rectangle &rectangle);
    virtual ~Image();
};
    
#endif /* Image_h */
