//
//  Animation.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 05/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "Animation.h"

/*!
 @param state Sets the animation active or not
 @discussion Sets the animation active or dormant. An animation should be active and added to the animation list in the screen before calling Start
 */
void Animation::SetActive(bool state) noexcept
{
    mIsActive = state;
}

/*!
 @return Returns true or false representing whether the animation is active or not respectively
 @discussion Tells whether the animation is active or not
*/
bool Animation::IsActive() const noexcept
{
    return mIsActive;
}

/*!
 @param state Enable or disable looping
 @discussion Tells the engine to run this animation in loop
 */
void Animation::SetLoop(bool state) noexcept
{
    mLoop = state;
}

/*!
 @return Returns true is the animation would be exeucted in loop. False otherwise.
 @discussion Tells whether this animation would be executed in loop by the engine or not.
 */
bool Animation::IsLooped() const noexcept
{
    return mLoop;
}

/*!
 @discussion Virtual distructor to be overridden by the derived class
 */
Animation::~Animation()
{
    
}

