//
//  TextBox.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 02/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "TextBox.h"

TextBox::TextBox(string name, string str, const Colour &textColour, const Rectangle &rectangle):Object(name, rectangle)
{
    mHasBackgroundColour = false;
    mTextColour = textColour;
    Set(str);
}

TextBox::TextBox(string name, string str, const Colour &textColour, const Colour &backgroundColour, const Rectangle &rectangle):Object(name, rectangle)
{
    mHasBackgroundColour = true;
    mTextColour = textColour;
    mBackgroundColour = backgroundColour;
    Set(str);
}

void TextBox::Set(string str)
{
    /*A simple implementation of re-adjustable Text Box.
     More sophesticated one with a separate Font class can be impletmented
     to make it more generic. For now keeping it simple*/
    int i = 72;//str.length();
    while(i > 0)
    {
        unique_ptr<Surface> upText;
        if(mHasBackgroundColour)
        {
            upText = make_unique<Surface>(str, --i, mTextColour, mBackgroundColour);
        }
        else
        {
            upText = make_unique<Surface>(str, --i, mTextColour);
        }
        
        if(upText->GetWidth() < mRectangle.GetWidth() &&
        upText->GetHeight() < mRectangle.GetHeight())
        {
            Point p((mRectangle.GetWidth() - upText->GetWidth())/2, (mRectangle.GetHeight() - upText->GetHeight())/2);
            Rectangle newPostion(p, mRectangle.GetWidth(), mRectangle.GetHeight());
            
            shared_ptr<Surface> newSurface = make_shared<Surface>(mRectangle.GetWidth(), mRectangle.GetHeight());
            if(mHasBackgroundColour)
            {
                newSurface->FillRectangle(mBackgroundColour);
            }
            newSurface->Blit(*upText.get(), newPostion);
            
            mspSurface = newSurface;
            break;
        }
    }
}

TextBox::~TextBox()
{
}

