//
//  Object.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 06/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Object.h"

Object::Object(string name, const Rectangle &rectangle) noexcept
{
    mName = name;
    SetRectangle(rectangle);
}

string Object::GetName()
{
    return mName;
}

void Object::SetRectangle(const Rectangle &rectangle) noexcept
{
    mRectangle = rectangle;
}

/*TODO: Why did we make this not return a &*/
Rectangle Object::GetRectangle()const noexcept
{
    return mRectangle;
}

bool Object::IsActive() const noexcept
{
    return mIsActive;
}

void Object::SetActive(bool state) noexcept
{
    mIsActive = state;
}

//bool Object::operator<(const Object &object) const noexcept
//{
//    return(this->mZIndex < object.mZIndex);
//}

void Object::SetZIndex(int zIndex) noexcept
{
    mZIndex = zIndex;
}

Object::~Object()
{
    
}

