//
//  Animation.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 05/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Animation_h
#define Animation_h

#include <vector>
#include <functional>
using namespace std;


/*!
 @class Animation
 @discussion All the animations should be inherited from this class.
 */
class Animation
{
    friend class Window;
private:
    
protected:
    bool mIsActive = false;
    bool mLoop = false;
public:
    /*These will be replaced by Event class in future*/
    std::function<void(void)> mOnReStart;
    std::function<void(void)> mOnEnd;
    
    virtual void Start() = 0;
    void SetActive(bool state) noexcept;
    bool IsActive() const noexcept;
    void SetLoop(bool state) noexcept;
    bool IsLooped() const noexcept;
    /*!
     @return bool: Returns false if the animation is over and true otherwise.
     @discussion This function has the responsibility to tell the engine when the animation is over.
     It has to be implemented by the derived class.
     */
    virtual bool Move() = 0;
    virtual ~Animation();
};

#endif /* Animation_h */
