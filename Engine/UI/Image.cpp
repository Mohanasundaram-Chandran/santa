//
//  Image.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 06/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Image.h"

Image::Image(string name, string path, const Rectangle &rectangle):Object(name, rectangle)
{
    mspSurface = std::make_shared<Surface>(path);
}

Image::Image(string name, const Colour &colour, const Rectangle &rectangle):Object(name, rectangle)
{
    mspSurface = std::make_shared<Surface>(colour, rectangle.GetWidth(), rectangle.GetHeight());
}

Image::~Image()
{
    
}

