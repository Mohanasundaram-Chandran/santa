//
//  Screen.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef Screen_h
#define Screen_h

#include <SDL2/SDL.h>
#include <vector>
#include <memory>
#include <iostream>

#include "Object.h"
#include "Animation.h"
#include "Image.h"
#include "../Kernel/GestureDetector.h"

using namespace std;

class Engine;

class Screen
{
    friend class Engine;
private:
    bool ShouldEndScreenAtTheEndOfThisFrame() const noexcept;

protected:
    // Rectangle mRectangle;
    // shared_ptr<Image> mspBackgroundImage;
    // void SetBackground(shared_ptr<Image> spBackgroundImage) noexcept;
    // void SetBackground(Colour backgroundColour) noexcept;

public:
    vector<shared_ptr<Object>> mChildObjects;//A handy list to display objects.
    vector<shared_ptr<Animation>> mAnimations;//A handy list to Animations.
    bool mShouldEndScreenAtTheEndOfThisFrame = false;
    
    // Screen(Rectangle rectangle);
    // shared_ptr<Image> GetBackgroundImage() const noexcept;
    virtual void OnLoad(){};
    virtual void BeforeDrawingEveryFrame() = 0;
    virtual void BeforeQuittingApplication(){};
    virtual void OnUnload(){};
    void AddObjectToDisplayList(shared_ptr<Object> object) noexcept;
    void RemoveObjectFromDisplayList(shared_ptr<Object> object) noexcept;
    void AddAnimationToList(shared_ptr<Animation> animation) noexcept;
    void RemoveAnimationFromList(shared_ptr<Animation> animation) noexcept;
    
    /*We could make these virtual functions and expect the derived class to override it.
     However the way to add handlers in Screen would be different from adding in Objects.
     Would like to keep it consistant across the engine.
     This also gives us the opportunity to check for handler and then call it.
     */
    function<void(Point)> mOnMouseButtonDown;
    function<void(Point)> mOnMouseButtonUp;
    function<void(Point)> mOnMouseOver;
    function<void(Point)> mOnMouseButtonClick;
    function<void(Point, Point)> mOnDrag;
    function<void(Point, Point)> mOnDrop;
    
    //            Screen();
    
    //Except ExlodeAnimation all other animations need improvement to make them more generic
    //            void ExplodeAnimation(Object *pObject, int speed, SDL_Surface *pBackground);
    //            void MoveAnimation(Object * pObject, SDL_Rect * pPosition, size_t hopCount, SDL_Surface *pBackground);
    //            void ImplodeAnimation(Object *pObject, int speed, unsigned int backgroundColour);
    //            void ImplodeAnimation(vector<Object *> objects, int speed, unsigned int backgroundColour);
    //            void PulseAnimation(Object *pObject, int speed);
};
#endif /* Screen_h */
