//
//  TextBox.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 02/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

//A simple implemenation of text box which adjusts the size of the conent to fit the text box size

#ifndef TextBox_h
#define TextBox_h

#include <stdexcept>
#include <string>

#include "Object.h"
#include "../SDL/Rectangle.h"
#include "../SDL/Colour.h"

using namespace std;

class TextBox: public Object
{
private:
    bool mHasBackgroundColour = false;
    Colour mTextColour {0,0,0};
    Colour mBackgroundColour {0,0,0};
    
public:
    /*
     1. All these functions throw TextRenderingFailedException which can be handled
     by the user code. Hence can not be noexcept
     2. All these functions set the member variable mspSurface hence can not be const.
     */
    TextBox(string name, string str, const Colour &textColour, const Rectangle &rectangle);
    TextBox(string name, string str, const Colour &textColour, const Colour &backgroundColour, const Rectangle &rectangle);
    void Set(string text);
    virtual ~TextBox();
};

#endif /* TextBox_h */
