//
//  Screen.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Screen.h"

// Screen::Screen(Rectangle rectangle):mRectangle(rectangle)
// {

// }

//Screen::Screen()
//{
//    //Discard the pending events
//    //SDL_FlushEvents does not seem to work as intended
//    bool isEventQueueFlushed = false;
//    while(!isEventQueueFlushed)
//    {
//        GestureReader::Gesture &gesture = mGestureReader.Read();
//        switch(gesture.mAction)
//        {
//            case GestureReader::Gesture::Action::None:
//                isEventQueueFlushed = true;
//                break;
//        }
//    }
//}

void Screen::AddObjectToDisplayList(std::shared_ptr<Object> object) noexcept
{
    std::cout<<"Screen::AddObjectToDisplayList:Object Name:"<<object->GetName()<<std::endl;
    mChildObjects.push_back(object);
}

void Screen::RemoveObjectFromDisplayList(std::shared_ptr<Object> object) noexcept
{
    auto foundElement = std::find(mChildObjects.begin(), mChildObjects.end(), object);
    if(foundElement != mChildObjects.end())
    {
        mChildObjects.erase(foundElement);
    }
}

void Screen::AddAnimationToList(std::shared_ptr<Animation> animation) noexcept
{
    mAnimations.push_back(animation);
}

void Screen::RemoveAnimationFromList(std::shared_ptr<Animation> animation) noexcept
{
    auto foundElement = std::find(mAnimations.begin(), mAnimations.end(), animation);
    if(foundElement != mAnimations.end())
    {
        mAnimations.erase(foundElement);
    }
}

bool Screen::ShouldEndScreenAtTheEndOfThisFrame() const noexcept
{
    return mShouldEndScreenAtTheEndOfThisFrame;
}

// void Screen::SetBackground(std::shared_ptr<Image> spBackgroundImage) noexcept
// {
//     mspBackgroundImage = spBackgroundImage;
// }

// void Screen::SetBackground(Colour backgroundColour) noexcept
// {
//     mspBackgroundImage = std::make_shared<Image>("Background Colour", backgroundColour, mRectangle);
// }

// shared_ptr<Image> Screen::GetBackgroundImage() const noexcept
// {
//     return mspBackgroundImage;
// }

////Taking the background gives lot of room for optimisation
////It avoids redrawing the board using the objects
//void Screen::ExplodeAnimation(Object *pObject, int speed, SDL_Surface *pBackground)
//{
//    if(!pObject)
//    {
//        return;
//    }
//    
//    /*Starting Rectangle*/
//    SDL_Rect startingRectangle;
//    startingRectangle.x = pObject->mPosition.x + (pObject->mPosition.w/2);
//    startingRectangle.y = pObject->mPosition.y + (pObject->mPosition.h/2);
//    startingRectangle.w = 0;
//    startingRectangle.h = 0;
//    
//    int maxValue = pObject->mPosition.w > pObject->mPosition.h? pObject->mPosition.w : pObject->mPosition.h;
//    /*Now explode*/
//    for(int i=0;i<maxValue; i+=speed)
//    {
//        SDL_Rect targetRectangle;
//        if((startingRectangle.w + i) < pObject->mPosition.w)
//        {
//            targetRectangle.x = startingRectangle.x - (i/2);
//            targetRectangle.w = startingRectangle.w + i;
//        }
//        if((startingRectangle.h + i) < pObject->mPosition.h)
//        {
//            targetRectangle.y = startingRectangle.y - (i/2);
//            targetRectangle.h = startingRectangle.h + i;
//        }
//        
//        if(pBackground)
//        {
//            Paint(pBackground);
//        }
//        Paint(pObject);
//        DonePainting();
//    }
//}
//
//void Screen::MoveAnimation(Object * pObject, SDL_Rect * pPosition, size_t hopCount, SDL_Surface *pBackground)
//{
//    if(!pObject ||
//       !pPosition)
//    {
//        return;
//    }
//    
//    do
//    {
//        if(pObject->mPosition.x < pPosition->x)
//        {
//            //Increment x
//            if((pPosition->x - pObject->mPosition.x) < hopCount)
//            {
//                hopCount = pPosition->x - pObject->mPosition.x;
//            }
//            pObject->mPosition.x+=hopCount;
//        }
//        else if(pObject->mPosition.x > pPosition->x)
//        {
//            //Decrement x - Incase if we decide to keep the basket to the left side of the board
//            if((pObject->mPosition.x - pPosition->x) < hopCount)
//            {
//                hopCount = pObject->mPosition.x - pPosition->x;
//            }
//            pObject->mPosition.x-=hopCount;
//        }
//        if(pObject->mPosition.y < pPosition->y)
//        {
//            //Increment y
//            if((pPosition->y - pObject->mPosition.y) < hopCount)
//            {
//                hopCount = pPosition->y - pObject->mPosition.y;
//            }
//            pObject->mPosition.y+=hopCount;
//        }
//        else if(pObject->mPosition.y > pPosition->y)
//        {
//            //Decrement y - Incase if we decide to keep the basket to the left side of the board
//            if((pObject->mPosition.y - pPosition->y) < hopCount)
//            {
//                hopCount = pObject->mPosition.y - pPosition->y;
//            }
//            pObject->mPosition.y-=hopCount;
//        }
//        
//        if(pBackground)
//        {
//            Paint(pBackground);
//        }
//        Paint(pObject);
//        DonePainting();
//    }while(pObject->mPosition.x != pPosition->x ||
//           pObject->mPosition.y != pPosition->y);
//}
//
//void Screen::ImplodeAnimation(vector<Object *> objects, int speed, unsigned int backgroundColour)
//{
//    if(objects.size() == 0)
//    {
//        return;
//    }
//    
//    for(int i=0;i<objects[0]->mPosition.w; i+=speed)
//    {
//        for(Object *pObject:objects)
//        {
//            SDL_Rect sourceRectangle;
//            sourceRectangle.x = pObject->mPosition.x+i;
//            sourceRectangle.y = pObject->mPosition.y+i;
//            sourceRectangle.w = pObject->mPosition.w-(i*2);
//            sourceRectangle.h = pObject->mPosition.h-(i*2);
//            
//            SDL_Rect targetRectangle;
//            targetRectangle.x = sourceRectangle.x;
//            targetRectangle.y = sourceRectangle.y;
//            targetRectangle.w = sourceRectangle.w;
//            targetRectangle.h = sourceRectangle.h;
//            
//            Paint(backgroundColour, pObject->mPosition);
//            Paint(pObject);
//            DonePainting();
//        }
//    }
//}
//
//void Screen::ImplodeAnimation(Object *pObject, int speed, unsigned int backgroundColour)
//{
//    if(!pObject)
//    {
//        return;
//    }
//    
//    for(int i=0;i<pObject->mPosition.w; i+=speed)
//    {
//        SDL_Rect targetRectangle;
//        targetRectangle.x = pObject->mPosition.x+i;
//        targetRectangle.y = pObject->mPosition.y+i;
//        targetRectangle.w = pObject->mPosition.w-(i*2);
//        targetRectangle.h = pObject->mPosition.h-(i*2);
//        
//        Paint(backgroundColour, pObject->mPosition);
//        Paint(pObject);
//        DonePainting();
//    }
//}
//
//void Screen::PulseAnimation(Object *pObject, int speed)
//{
//    if(!pObject)
//    {
//        return;
//    }
//    
//    for(int i=0;i<pObject->mPosition.w; i+=speed)
//    {
//        SDL_Rect sourceRectangle;
//        sourceRectangle.x = pObject->mPosition.x+i;
//        sourceRectangle.y = pObject->mPosition.y+i;
//        sourceRectangle.w = pObject->mPosition.w-(i*2);
//        sourceRectangle.h = pObject->mPosition.h-(i*2);
//        
//        SDL_Rect targetRectangle;
//        targetRectangle.x = sourceRectangle.x;
//        targetRectangle.y = sourceRectangle.y;
//        targetRectangle.w = sourceRectangle.w;
//        targetRectangle.h = sourceRectangle.h;
//        
//        Paint(pObject);
//        DonePainting();
//    }
//    
//    for(int i=pObject->mPosition.w;i>=0; i-=speed)
//    {
//        SDL_Rect sourceRectangle;
//        sourceRectangle.x = pObject->mPosition.x + (i/2);
//        sourceRectangle.y = pObject->mPosition.y + (i/2);
//        sourceRectangle.w = pObject->mPosition.w - (i);
//        sourceRectangle.h = pObject->mPosition.h - (i);
//        
//        SDL_Rect targetRectangle;
//        targetRectangle.x = sourceRectangle.x;
//        targetRectangle.y = sourceRectangle.y;
//        targetRectangle.w = sourceRectangle.w;
//        targetRectangle.h = sourceRectangle.h;
//        
//        Paint(pObject);
//        DonePainting();
//    }
//}
//
//void Screen::TakeSnapshot()
//{
//    SDL_FreeSurface(mpSnapshot);
//    mpSnapshot = Engine::GetWindow().TakeSnapshot();
//}


