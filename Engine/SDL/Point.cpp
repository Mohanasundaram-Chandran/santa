//
//  Point.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "Point.h"

Point::Point(int x, int y) noexcept
{
    mX = x;
    mY = y;
}

int Point::GetX() const noexcept
{
    return mX;
}

void Point::SetX(int x) noexcept
{
    mX = x;
}

int Point::GetY() const noexcept
{
    return mY;
}

void Point::SetY(int y) noexcept
{
    mY = y;
}

bool Point::operator==(const Point &point) const noexcept
{
    if(mX == point.GetX() &&
       mY == point.GetY())
    {
        return true;
    }
    else
    {
        return false;
    }
}

