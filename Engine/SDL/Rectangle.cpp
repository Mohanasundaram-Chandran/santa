//
//  Rectangle.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "Rectangle.h"

/*This function will only be called by Surface and Window*/
SDL_Rect *Rectangle::GetSDLObject() const noexcept
{
    return mupRect.get();
}

Rectangle::Rectangle() noexcept
{
    Set(Point(0, 0), 0, 0);
}

Rectangle::Rectangle(const Point &point, int width, int height) noexcept
{
    Set(point, width, height);
}

Rectangle::Rectangle(const Rectangle &rectangle) noexcept
{
    Set(rectangle.GetPoint(), rectangle.GetWidth(), rectangle.GetHeight());
}

void Rectangle::operator = (const Rectangle &rectangle)
{
    Set(rectangle.GetPoint(), rectangle.GetWidth(), rectangle.GetHeight());
}

void Rectangle::Set(const Point &point, int width, int height) noexcept
{
    if(!mupRect)
    {
        mupRect = make_unique<SDL_Rect>();
    }
    mupRect->x = point.GetX();
    mupRect->y = point.GetY();
    mupRect->w = width;
    mupRect->h = height;
}

Point Rectangle::GetPoint() const noexcept
{
    return Point(mupRect->x, mupRect->y);
}

bool Rectangle::operator==(const Rectangle &rectangle)
{
    if (mupRect->x == rectangle.GetPoint().GetX() &&
        mupRect->y == rectangle.GetPoint().GetY() &&
        mupRect->w == rectangle.GetWidth() &&
        mupRect->h == rectangle.GetHeight())
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Rectangle::GetX() const noexcept
{
    return mupRect->x;
}

int Rectangle::GetY() const noexcept
{
    return mupRect->y;
}

void Rectangle::SetX(int x) noexcept
{
    mupRect->x = x;
}

void Rectangle::SetY(int y) noexcept
{
    mupRect->y = y;
}

void Rectangle::SetWidth(int width) noexcept
{
    mupRect->w = width;
}

void Rectangle::SetHeight(int height) noexcept
{
    mupRect->h = height;
}

int Rectangle::GetWidth() const noexcept
{
    return mupRect->w;
}

int Rectangle::GetHeight() const noexcept
{
    return mupRect->h;
}
