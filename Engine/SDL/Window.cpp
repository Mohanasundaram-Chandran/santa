#include "Window.h"

Window::Window(int width, int height, string title)
{
    /*Create the main window on which the game board is drawn*/
    if(SDL_InitSubSystem(SDL_INIT_VIDEO) < 0)
    {
        throw WindowInitFailedException();
    }
    
    mpWindow = SDL_CreateWindow( title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN );
    if(nullptr == mpWindow)
    {
        throw WindowInitFailedException();
    }
    
    SDL_FillRect(SDL_GetWindowSurface(mpWindow), nullptr, 0xFFFFFF);//Paint the background White
    SDL_UpdateWindowSurface(mpWindow);
    /*Basic window with white background ready*/
}

SDL_PixelFormat * Window::GetPixelFormat() const noexcept
{
    return SDL_GetWindowSurface(mpWindow)->format;
}

void Window::SetTitle(string title) noexcept
{
    SDL_SetWindowTitle(mpWindow, title.c_str());
}

void Window::UpdateSurface()
{
    /*Update the screen*/
    SDL_UpdateWindowSurface(mpWindow);
}

SDL_Window * Window::GetSDLObject() const noexcept
{
    return mpWindow;
}

Window::~Window() noexcept
{
    SDL_DestroyWindow(this->mpWindow);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}
