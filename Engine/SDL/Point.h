//
//  Point.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Point_h
#define Point_h

class Point
{
private:
    int mX = 0;
    int mY = 0;
public:
    /*Some, like everything to be encapsulated*/
    Point(int x, int y) noexcept;
    int GetX() const noexcept;
    void SetX(int x) noexcept;
    int GetY() const noexcept;
    void SetY(int y) noexcept;
    bool operator==(const Point &point) const noexcept;
};

#endif /* Point_h */
