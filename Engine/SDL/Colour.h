//
//  Colour.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Colour_h
#define Colour_h

#include <SDL2/SDL.h>
// #include "TextBox.h"

class Colour
{
    // friend class TextBox;
private:
    SDL_Colour mColour;
public:
    Colour(int red = 255, int green = 255, int blue = 255, int alpha = 0) noexcept;
    int GetRed() const noexcept;
    int GetGreen() const noexcept;
    int GetBlue() const noexcept;
    int GetAlpha() const noexcept;
    SDL_Colour GetSDLObject() const noexcept;
};

#endif /* Colour_h */
