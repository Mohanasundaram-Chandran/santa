//
//  Surface.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 03/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Surface_h
#define Surface_h

#include <SDL2/SDL.h>
#include <string>

#include "Colour.h"
#include "Rectangle.h"

using namespace std;

class Surface
{
private:
    SDL_Surface * mpSurface = nullptr;
public:
    static SDL_PixelFormat * mpPixelFormat;
    class LoadingImageFailedException{};
    class CreatingSurfaceFailedException{};
    class TextRenderingFailedException{};
    
    Surface(string path);
    Surface(int width, int height) noexcept;
    Surface(const Colour &colour, int width, int height) noexcept;
    Surface(string text, int fontSize, const Colour &textColour, const Colour &backgroundColour);
    Surface(string text, int fontSize, const Colour &textColour);
    
    int GetWidth() const noexcept;
    int GetHeight() const noexcept;
    void FillRectangle(const Colour &mBackgroundColour) noexcept;
    void Blit(const Surface &surface, const Rectangle &rectanlge) noexcept;

    SDL_Surface * GetSDLObject() const noexcept;
    
    ~Surface() noexcept;
};

#endif /* Surface_h */
