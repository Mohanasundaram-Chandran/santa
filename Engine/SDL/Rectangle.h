//
//  Rectangle.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Rectangle_h
#define Rectangle_h

#include <memory>
#include <SDL2/SDL.h>

using namespace std;

#include "Point.h"

class Rectangle
{
private:
    unique_ptr<SDL_Rect> mupRect;
public:
    Rectangle() noexcept;
    Rectangle(const Point &point, int width, int height) noexcept;
    Rectangle(const Rectangle &rectangle) noexcept;
    void operator = (const Rectangle &);
    void Set(const Point &point, int width, int height) noexcept;
    Point GetPoint() const noexcept;
    int GetX() const noexcept;
    int GetY() const noexcept;
    int GetWidth() const noexcept;
    int GetHeight() const noexcept;

    void SetX(int x) noexcept;
    void SetY(int y) noexcept;
    void SetWidth(int width) noexcept;
    void SetHeight(int height) noexcept;
    bool operator==(const Rectangle & rectangle);
    SDL_Rect * GetSDLObject() const noexcept;
};

#endif /* Rectangle_h */
