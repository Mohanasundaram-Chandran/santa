//
//  Colour.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 08/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "Colour.h"

SDL_Colour Colour::GetSDLObject() const noexcept
{
    return mColour;
}

Colour::Colour(int red, int green, int blue, int alpha) noexcept
{
    mColour.r = red;
    mColour.g = green;
    mColour.b = blue;
    mColour.a = alpha;
}

int Colour::GetRed() const noexcept
{
    return mColour.r;
}

int Colour::GetGreen() const noexcept
{
    return mColour.g;
}

int Colour::GetBlue() const noexcept
{
    return mColour.b;
}

int Colour::GetAlpha() const noexcept
{
    return mColour.a;
}
