//
//  Surface.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 03/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include <SDL2_ttf/SDL_ttf.h>

#include "Surface.h"

SDL_PixelFormat* Surface::mpPixelFormat = nullptr;

/*The following function would only be called by Window*/
SDL_Surface * Surface::GetSDLObject() const noexcept
{
    return mpSurface;
}

Surface::Surface(string path)
{
    SDL_Surface * pLoadedSurface = SDL_LoadBMP(path.c_str());
    SDL_Surface * pConvertedSurface = SDL_ConvertSurface(pLoadedSurface, mpPixelFormat, 0);
    SDL_FreeSurface(pLoadedSurface);
    
    if(!pConvertedSurface)
    {
        throw LoadingImageFailedException();
    }
    
    mpSurface = pConvertedSurface;
}

Surface::Surface(int width, int height) noexcept
{
    SDL_Surface *pSurface = SDL_CreateRGBSurface(0, width, height,
        mpPixelFormat->BitsPerPixel,
        mpPixelFormat->Rmask, mpPixelFormat->Gmask,
        mpPixelFormat->Bmask, mpPixelFormat->Amask );
    SDL_Surface *pConvertedSurface = SDL_ConvertSurface(pSurface, mpPixelFormat, 0);
    SDL_FreeSurface(pSurface);
    
    mpSurface = pConvertedSurface;
}

Surface::Surface(const Colour &colour, int width, int height) noexcept
{
    Surface(width, height);
    FillRectangle(colour);
}

/*
 * Cpp Core Guideline (https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#f51-where-there-is-a-choice-prefer-default-arguments-over-overloading)
 * F.51: Where there is a choice, prefer default arguments over overloading
 * Can not honour this guideline for the reason that the internal TTF function also varies depending
 * on the number of parameters and no way to find if the backgroundColour is passed or not. Just for that
 * it should be made a pointer. Did not sound clean. So chose to do otherway.
 */

Surface::Surface(string text, int fontSize, const Colour &textColour, const Colour &backgroundColour)
{
    TTF_Font * pFont = TTF_OpenFont("Fonts/Brush Script.ttf", fontSize);
    mpSurface = TTF_RenderText_Shaded(pFont, text.c_str(), textColour.GetSDLObject(), backgroundColour.GetSDLObject());
    TTF_CloseFont(pFont);
    if(!mpSurface)
    {
        throw TextRenderingFailedException();
    }
}

Surface::Surface(string text, int fontSize, const Colour &textColour)
{
    TTF_Font * pFont = TTF_OpenFont("Fonts/Brush Script.ttf", fontSize);
    mpSurface = TTF_RenderText_Solid(pFont, text.c_str(), textColour.GetSDLObject());
    TTF_CloseFont(pFont);
    if(!mpSurface)
    {
        throw TextRenderingFailedException();
    }
}

void Surface::FillRectangle(const Colour &mBackgroundColour) noexcept
{
    SDL_FillRect(GetSDLObject(), NULL, SDL_MapRGB(mpPixelFormat, mBackgroundColour.GetRed(), mBackgroundColour.GetGreen(), mBackgroundColour.GetBlue()));
}

void Surface::Blit(const Surface &sourceSurface, const Rectangle &rectanlge) noexcept
{
    SDL_BlitSurface(sourceSurface.GetSDLObject(), NULL, GetSDLObject(), rectanlge.GetSDLObject());
}

int Surface::GetWidth() const noexcept
{
    return GetSDLObject()->w;
}

int Surface::GetHeight() const noexcept
{
    return GetSDLObject()->h;
}

Surface::~Surface() noexcept
{
    SDL_FreeSurface(GetSDLObject());
}

