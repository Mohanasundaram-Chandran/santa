#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>
#include <string>
using namespace std;

class Window
{
private:
    SDL_Window * mpWindow = NULL;//SDL window the class represents

    SDL_PixelFormat * GetPixelFormat() const noexcept;//Only used by Engine class and Surface class
public:
    Window(int width, int height, string title);//Only the Engine class can create an object to this class    
    class WindowInitFailedException{};
    void SetTitle(string title) noexcept;
    void UpdateSurface();
    SDL_Window * GetSDLObject() const noexcept;    
    ~Window() noexcept;    
};
#endif