//
//  FontManager.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 10/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef FontManager_h
#define FontManager_h

#include <SDL2_ttf/SDL_ttf.h>

class FontManager
{
public:
    class FontSubSystemInitialisationFailedException{};
    FontManager();
    ~FontManager();
};

#endif /* FontManager_h */
