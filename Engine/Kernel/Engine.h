//
//  Engine.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 27/05/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Engine_h
#define Engine_h

#include <SDL2/SDL.h>
#include <string>
#include <memory>
#include <chrono>
#include <iostream>

#include "../SDL/Window.h"
#include "../UI/Screen.h"
#include "AudioManager.h"
#include "FontManager.h"

using namespace std;

/*If we implement sleep using SDL_Delay(),
 then it will affect the frame rate.
 If we decide to take actions based on frame rate,
 then it would be incorrect.
 Hence implementing sleep in a way it does not affect
 frame rate
 */
#define YIELD(milliSeconds)                                                                                        \
    {                                                                                                              \
        static std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();                 \
        std::chrono::system_clock::time_point currentTime = std::chrono::system_clock::now();                      \
        auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count(); \
        std::cout<<"elapsedTime:"<<elapsedTime<<"milliSeconds:"<<(milliSeconds)<<std::endl;                        \
        if (elapsedTime < (milliSeconds))                                                                          \
        {                                                                                                          \
            return;                                                                                                \
        }                                                                                                          \
    }

class Engine
{
  private:
    static shared_ptr<Window> spWindow; /*Represents the OS window with bar containing close and resize buttons*/
    static shared_ptr<AudioManager> spAudioManager;
    static shared_ptr<FontManager> spFontManager;

    int mTargetFramesPerSecond = 60; //TODO: Find out the underlying hardware's refresh rate and set as the value.
    void ProcessPendingEvents(Screen &screen) noexcept;
    GestureDetector mGestureDetector;

  public:
    class EngineInitFailedException
    {
    };

    Engine(int windowWidth, int windowHeight, string windowTitle);
    static shared_ptr<Window> GetWindow() noexcept;
    static shared_ptr<AudioManager> GetAudioManager() noexcept;
    static shared_ptr<FontManager> GetFontManager() noexcept;

    void DisplayScreen(Screen &screen) noexcept;
    int GetTargetFrameRate() const noexcept;
    void SetTargetFrameRate(int framesPerSecond) noexcept;
    unique_ptr<Image> TakeScreenshot();

    /*This function is used by the Window class to paint the objects in the screen to it's own surface*/
    void PaintSurface(shared_ptr<Surface> spSurface, const Rectangle &targetRectangle) const noexcept;

    /*This function is used by the Window class to paint the objects in the screen to it's own surface*/
    void PaintSurface(int colour, const Rectangle &targetRectangle) const noexcept;

    ~Engine() noexcept;
};

#endif /* Engine_h */
