//
//  Event.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 02/09/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef Event_h
#define Event_h

/*
 This event class should support multiple handlers.
 Variadic tempaltes for std::function declaration.
 */
    class Event
    {
    private:
        
    };

#endif /* Event_h */
