//
//  AudioManager.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 20/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef AudioManager_h
#define AudioManager_h

#include <string>
#include <vector>
#include <map>
using namespace std;

#include <SDL2/SDL.h>
#include <SDL2_mixer/SDL_mixer.h>

class AudioManager
{
    Mix_Music *mpMusic = NULL;
    map<string, Mix_Music *> mMusicNumbers;
    map<string, Mix_Chunk *> mSoundEffects;
    bool mIsMusicOn = true;
    bool mIsSoundEffectsOn = true;
    
    Mix_Music *GetMusic(string musicPath);
    Mix_Chunk *GetSoundEffect(string soundEffectPath);
    
public:
    static const int MaxVolume = MIX_MAX_VOLUME;

    class AudioSubSystemInitialistionFailed{};
    class LoadingMusicFailedException{};
    class LoadingSoundEffectFailedException{};
    class PlayingMusicFailedException{};
    class PlayingSoundEffectFailedException{};
    
    AudioManager();
    void SetMusicOn() noexcept;
    void SetMusicOff() noexcept;
    void ToggleMusicState() noexcept;
    bool MusicState() const noexcept;

    void SetSoundEffectsOn() noexcept;
    void SetSoundEffectsOff() noexcept;
    void ToggleSoundEffectsState() noexcept;
    bool SoundEffectsState() const noexcept;
    
    void LoadMusic(string musicPath);
    void PlayMusic(string musicPath, int volume);
    void PauseMusic() const noexcept;
    void ResumeMusic() const noexcept;
    void StopMusic() const noexcept;
    void UnLoadMusic(string musicPath) noexcept;
    
    void LoadSoundEffect(string soundEffectPath);
    void PlaySoundEffect(string soundEffectPath, int volume) noexcept;
    void UnLoadSoundEffect(string soundEffectPath) noexcept;
    ~AudioManager() noexcept;
};

#endif /* AudioManager_h */
