//
//  AudioManager.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 20/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "AudioManager.h"
AudioManager::AudioManager()
{
    /*Init the library system*/
    if(SDL_InitSubSystem(SDL_INIT_AUDIO) < 0)
    {
        throw AudioSubSystemInitialistionFailed();
    }
    
    //Initialize SDL_mixer
    Mix_Init(0);
    if(-1 == Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096))
    {
        throw AudioSubSystemInitialistionFailed();
    }
    Mix_Volume(-1,MIX_MAX_VOLUME/4);
}

void AudioManager::SetMusicOn() noexcept
{
    mIsMusicOn = true;
    ResumeMusic();
}

void AudioManager::SetMusicOff() noexcept
{
    PauseMusic();
    mIsMusicOn = false;
}

void AudioManager::ToggleMusicState() noexcept
{
    if(mIsMusicOn)
    {
        SetMusicOff();
    }
    else
    {
        SetMusicOn();
    }
}

bool AudioManager::MusicState() const noexcept
{
    return mIsMusicOn;
}

void AudioManager::SetSoundEffectsOn() noexcept
{
    mIsSoundEffectsOn = true;
}

void AudioManager::SetSoundEffectsOff() noexcept
{
    mIsSoundEffectsOn = false;
}

void AudioManager::ToggleSoundEffectsState() noexcept
{
    if(mIsSoundEffectsOn)
    {
        SetSoundEffectsOff();
    }
    else
    {
        SetSoundEffectsOn();
    }
}

bool AudioManager::SoundEffectsState() const noexcept
{
    return mIsSoundEffectsOn;
}

void AudioManager::LoadMusic(string musicPath)
{
    map<string, Mix_Music *>::iterator it = mMusicNumbers.find(musicPath);
    if(it == mMusicNumbers.end())
    {
        Mix_Music *pMusic = NULL;
        pMusic = Mix_LoadMUS(musicPath.c_str());
        if(!pMusic)
        {
            throw LoadingMusicFailedException();
        }
        mMusicNumbers[musicPath] = pMusic;
    }
}

Mix_Music * AudioManager::GetMusic(string musicPath)
{
    Mix_Music *pMusic = NULL;
    map<string, Mix_Music *>::iterator it = mMusicNumbers.find(musicPath);
    if(it == mMusicNumbers.end())
    {
        pMusic = Mix_LoadMUS(musicPath.c_str());
        if(!pMusic)
        {
            throw LoadingMusicFailedException();
        }
        mMusicNumbers[musicPath] = pMusic;
    }
    else
    {
        pMusic = it->second;
    }
    return pMusic;
}

void AudioManager::PlayMusic(string musicPath, int volume)
{
    if(!mIsMusicOn)
    {
        return;
    }
    
    Mix_Music *pMusic = GetMusic(musicPath);
    StopMusic();//Stop the currently playing music
    Mix_VolumeMusic(volume<MIX_MAX_VOLUME?volume:MIX_MAX_VOLUME);
    if(-1 == Mix_PlayMusic(pMusic, -1))
    {
        throw PlayingMusicFailedException();
    }
}

void AudioManager::PauseMusic() const noexcept
{
    if(!mIsMusicOn)
    {
        return;
    }
    
    Mix_PauseMusic();
}

void AudioManager::ResumeMusic() const noexcept
{
    if(!mIsMusicOn)
    {
        return;
    }
    
    Mix_ResumeMusic();
}

void AudioManager::StopMusic() const noexcept
{
    if(!mIsMusicOn)
    {
        return;
    }
    
    Mix_HaltMusic();
}

void AudioManager::UnLoadMusic(string musicPath) noexcept
{
    map<string, Mix_Music *>::iterator it = mMusicNumbers.find(musicPath);
    if(it != mMusicNumbers.end())
    {
        Mix_FreeMusic(it->second);
        mMusicNumbers.erase(it);
    }
}

void AudioManager::LoadSoundEffect(string soundEffectPath)
{
    map<string, Mix_Chunk *>::iterator it = mSoundEffects.find(soundEffectPath);
    if(it == mSoundEffects.end())
    {
        Mix_Chunk *pSoundEffect = Mix_LoadWAV(soundEffectPath.c_str());
        if(!pSoundEffect)
        {
            throw LoadingSoundEffectFailedException();
        }
        mSoundEffects[soundEffectPath] = pSoundEffect;
    }
}

Mix_Chunk *AudioManager::GetSoundEffect(string soundEffectPath)
{
    Mix_Chunk *pSoundEffect = NULL;
    map<string, Mix_Chunk *>::iterator it = mSoundEffects.find(soundEffectPath);
    if(it == mSoundEffects.end())
    {
        pSoundEffect = Mix_LoadWAV(soundEffectPath.c_str());
        if(!pSoundEffect)
        {
            throw LoadingSoundEffectFailedException();
        }
        mSoundEffects[soundEffectPath] = pSoundEffect;
    }
    else
    {
        pSoundEffect = it->second;
    }
    return pSoundEffect;
}

void AudioManager::PlaySoundEffect(string soundEffectPath, int volume) noexcept
{
    if(!mIsSoundEffectsOn)
    {
        return;
    }
    
    Mix_Chunk *pSoundEffect = GetSoundEffect(soundEffectPath);
    Mix_Volume(-1, volume<MIX_MAX_VOLUME?volume:MIX_MAX_VOLUME);//For now we play only one sound effect at a time. Hence just set all the channels to the same volume
    while(-1 == Mix_PlayChannel(-1, pSoundEffect, 0))
    {
        Mix_AllocateChannels(Mix_AllocateChannels(-1) + 1);
        Mix_Volume(-1, volume<MIX_MAX_VOLUME?volume:MIX_MAX_VOLUME);
    }
}

void AudioManager::UnLoadSoundEffect(string soundEffectPath) noexcept
{
    map<string, Mix_Chunk *>::iterator it = mSoundEffects.find(soundEffectPath);
    if(it != mSoundEffects.end())
    {
        Mix_FreeChunk(it->second);
        mSoundEffects.erase(it);
    }
}

AudioManager::~AudioManager() noexcept
{
    StopMusic();
    
    for(auto& keyValuePair:mMusicNumbers)
    {
        Mix_FreeMusic(keyValuePair.second);
    }
    
    for(auto& keyValuePair:mSoundEffects)
    {
        Mix_FreeChunk(keyValuePair.second);
    }
    
    Mix_CloseAudio();
    Mix_Quit();
    SDL_QuitSubSystem(SDL_INIT_AUDIO);
}
