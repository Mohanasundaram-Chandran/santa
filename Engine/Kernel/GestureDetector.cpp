//
//  GestureDetector.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 13/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "GestureDetector.h"
GestureDetector::GestureDetector()
{
    mDummy.mAction = Gesture::Action::None;
    mGesture.mStartingPoint.SetX(0);
    mGesture.mStartingPoint.SetY(0);
    mGesture.mCurrentPoint.SetX(0);
    mGesture.mCurrentPoint.SetY(0);
}

GestureDetector::Gesture & GestureDetector::Input(SDL_Event e) noexcept
{
    int x = 0;
    int y = 0;
    
    switch(e.type)
    {
        case SDL_QUIT:
            /*If the user presses Command-Q or click close button*/
            mGesture.mAction = Gesture::Action::Quit;
            mGesture.mStartingPoint.SetX(0);
            mGesture.mStartingPoint.SetY(0);
            mGesture.mCurrentPoint.SetX(0);
            mGesture.mCurrentPoint.SetY(0);
            mPreviousActions.clear();
            return mGesture;
            break;//Redundent but helps. Puts the brain in auto pilot mode.
        case SDL_MOUSEBUTTONDOWN:
            mGesture.mAction = Gesture::Action::None;
            x = 0;
            y = 0;
            SDL_GetMouseState(&x, &y);
            
            mGesture.mStartingPoint.SetX(x);
            mGesture.mStartingPoint.SetY(y);
            mGesture.mCurrentPoint.SetX(x);
            mGesture.mCurrentPoint.SetY(y);
            
            mPreviousActions.clear();
            mPreviousActions.push_back(e.type);
            return mGesture;
            break;
        case SDL_MOUSEMOTION:
            if(mPreviousActions.size() > 0 &&
               SDL_MOUSEBUTTONDOWN == mPreviousActions[0])
            {
                mGesture.mAction = Gesture::Action::Drag;
                x = 0;
                y = 0;
                
                SDL_GetMouseState(&x, &y);
                mGesture.mCurrentPoint.SetX(x);
                mGesture.mCurrentPoint.SetY(y);
                
                if(mPreviousActions.size() < 2)
                {
                    mPreviousActions.push_back(e.type);
                }
                return mGesture;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            if(mPreviousActions.size() == 1)
            {
                mGesture.mAction = Gesture::Action::Click;
            }
            if(mPreviousActions.size() == 2)
            {
                mGesture.mAction = Gesture::Action::Drop;
            }
            x = 0;
            y = 0;
            
            SDL_GetMouseState(&x, &y);
            mGesture.mCurrentPoint.SetX(x);
            mGesture.mCurrentPoint.SetY(y);
            mPreviousActions.clear();
            return mGesture;
            break;
    }
    
    return mDummy;
}

