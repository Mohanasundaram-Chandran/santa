//
//  FontManager.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 10/07/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "FontManager.h"

FontManager::FontManager()
{
    if(-1 == TTF_Init())
    {
        throw FontSubSystemInitialisationFailedException();
    }    
}

FontManager::~FontManager()
{
    TTF_Quit();
}

