//
//  Engine.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 27/05/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include<iostream>
#include <SDL2/SDL.h>
#include "Engine.h"

shared_ptr<Window> Engine::spWindow = nullptr; /*Represents the OS window with bar containing close and resize buttons*/
shared_ptr<AudioManager> Engine::spAudioManager = nullptr;
shared_ptr<FontManager> Engine::spFontManager = nullptr;

/*This function is used by the Window class to paint the objects in the screen to it's own surface*/
void Engine::PaintSurface(shared_ptr<Surface> spSurface, const Rectangle &targetRectangle) const noexcept
{
    std::cout<<targetRectangle.GetX()<<","<<targetRectangle.GetY()<<","<<targetRectangle.GetWidth()<<","<<targetRectangle.GetHeight()<<std::endl;
    SDL_BlitScaled(spSurface->GetSDLObject(), NULL, SDL_GetWindowSurface(spWindow.get()->GetSDLObject()), targetRectangle.GetSDLObject());
}

/*This function is used by the Window class to paint the objects in the screen to it's own surface*/
void Engine::PaintSurface(int colour, const Rectangle &targetRectangle) const noexcept
{
    SDL_FillRect(SDL_GetWindowSurface(spWindow.get()->GetSDLObject()), targetRectangle.GetSDLObject(), colour);
}

Engine::Engine(int windowWidth, int windowHeight, string windowTitle)
{
    SDL_Init(0);
    
    spWindow = std::make_shared<Window>(windowWidth, windowHeight, windowTitle);
    spAudioManager = std::make_shared<AudioManager>();
    spFontManager = std::make_shared<FontManager>();
    Surface::mpPixelFormat = SDL_GetWindowSurface(spWindow->GetSDLObject())->format;
}

void Engine::SetTargetFrameRate(int targetFramesPerSecond) noexcept
{
    mTargetFramesPerSecond = targetFramesPerSecond;
}

int Engine::GetTargetFrameRate() const noexcept
{
    return mTargetFramesPerSecond;
}

std::shared_ptr<Window> Engine::GetWindow() noexcept
{
    return spWindow;
}

std::shared_ptr<AudioManager> Engine::GetAudioManager() noexcept
{
    return spAudioManager;
}

std::shared_ptr<FontManager> Engine::GetFontManager() noexcept
{
    return spFontManager;
}

Engine::~Engine() noexcept
{
    SDL_Quit();
}

/*
 This function does not throw exception.
 If any of the functions called by this function throw exception we are not handling it as we do not know
 what to do with that.
 Hence this function does not expect any exceptions to be thrown to be caught by the calling function.
 Hence this function is declared noexcept
 */
    bool Compare(shared_ptr<Object> obj1, shared_ptr<Object> obj2)
    {
        return (obj1->mZIndex <obj2->mZIndex);
    }
    
void Engine::DisplayScreen(Screen & screen) noexcept
{
    screen.mShouldEndScreenAtTheEndOfThisFrame = false;
    //TODO: Discard pending events
    screen.OnLoad();
    
    while(true)
    {
        time_t frameStartTime = time(NULL);
        
        /*
         Arrange the objects based on mZIndex in ascending order.
         */
        std::sort(screen.mChildObjects.begin(), screen.mChildObjects.end(), Compare);
        
        /*Handle all the events happened in the previous frame*/
        ProcessPendingEvents(screen);
        screen.BeforeDrawingEveryFrame();
        for(auto animation:screen.mAnimations)
        {
            if(animation->IsActive())
            {
                if(!animation->Move())
                {
                    if(animation->IsLooped())
                    {
                        if(animation->mOnEnd)
                        {
                            animation->mOnEnd();
                        }

                        if(animation->mOnReStart)
                        {
                            animation->mOnReStart();
                        }
                        animation->Start();
//                        animation->Move();
                    }
                    else
                    {
                        animation->SetActive(false);
                        if(animation->mOnEnd)
                        {
                            animation->mOnEnd();
                        }
                    }
                    
                }
            }
        }
        /*Remove the completed animations from the animation list for the following benefits
         1. If the animation was not a member object it would be released from the memory.
         2. Does not stop the user from reusing the animation. Just that they have to add it to the animation list again.
         */
        //TODO: If there is no use for seperate loops then merge it in the future.
//        auto it = screen.mAnimations.begin();
//        while(it != screen.mAnimations.end())
//        {
//            if(!(*it)->Move())
//            {
//                it = screen.mAnimations.erase(it);
//            }
//            else
//            {
//                ++it;
//            }
//        }
        
        // /*Paint the background*/
        // if(screen.GetBackgroundImage())
        // {
        //     PaintSurface(screen.GetBackgroundImage(), screen.GetBackgroundImage().mRectangle);
        // }
        
        /*Paint the objects*/
//        std::cout<<"Engine::DisplayScreen::PaintObjects"<<std::endl;
        for(shared_ptr<Object> object:screen.mChildObjects)
        {
            if(object->IsActive())
            {
//                std::cout<<"Engine::DisplayScreen::object->mName:" + object->GetName()<<std::endl;
                PaintSurface(object->mspSurface, object->GetRectangle());
            }
        }
//        std::cout<<"Engine::DisplayScreen::PaintedObjects"<<std::endl;
        
        /*Update the screen*/
        SDL_UpdateWindowSurface(spWindow.get()->GetSDLObject());
        
        /*Close the screen if marked so*/
        if(screen.ShouldEndScreenAtTheEndOfThisFrame())
        {
            break;
        }
        time_t frameEndTime = time(NULL);
        double timePerFrame = 1.0/mTargetFramesPerSecond;
        double deltaTime = difftime(frameEndTime, frameStartTime);
        double remainingTime = timePerFrame - deltaTime;
        if(remainingTime > 0)
        {
            unsigned long milliSecondsToDelay = remainingTime * 1000;
            SDL_Delay(milliSecondsToDelay);
        }
    }
    
    screen.OnUnload();
}

/*
 This function does not throw exception.
 If any of the functions called by this function throw exception we are not handling it as we do not know
 what to do with that.
 Hence this function does not expect any exceptions to be thrown to be caught by the calling function.
 Hence this function is declared noexcept
 */
void Engine::ProcessPendingEvents(Screen & screen) noexcept
{
    SDL_Event e;
    while(SDL_PollEvent(&e) != 0)
    {
        int x = 0;
        int y = 0;
        /*Process the basic events*/
        switch(e.type)
        {
            case SDL_QUIT:
                /*If the user presses Command-Q or click close button*/
                screen.BeforeQuittingApplication();
                exit(0);
                break;//Redundent but helps. Puts the brain in auto pilot mode.
            case SDL_MOUSEBUTTONDOWN:
            {
                SDL_GetMouseState(&x, &y);
                Point mouseButtonDownPoint(x, y);
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnMouseButtonDown)
                {
                    screen.mOnMouseButtonDown(mouseButtonDownPoint);
                }
                else
                {
                    /*If objects are stacked on the screen.
                     Then the top most one was the clicked on.
                     It will have the highest mZIndex.
                     Once the objects are sorted in ascending order
                     looking for the objects from the end of the vector
                     would find the top most object clicked on.*/
                    auto iterator = screen.mChildObjects.end();
                    /*iterator now is pointing to end which is not a real object.
                     Hence move it one position to make it point the real end object
                     */
                    --iterator;
                    for(; iterator != screen.mChildObjects.begin(); --iterator)
                    {
                        Rectangle rectangle = (*iterator)->GetRectangle();
                        Point point = rectangle.GetPoint();
                        
                        if(x >= point.GetX() &&
                           x <= (point.GetX() + rectangle.GetWidth()) &&
                           y >= point.GetY() &&
                           y <= (point.GetY() + rectangle.GetHeight()))
                        {
                            if((*iterator)->mOnMouseButtonDown)
                            {
                                (*iterator)->mOnMouseButtonDown(mouseButtonDownPoint);
                            }
                            break;
                        }
                    }
                }
            }
            break;
            case SDL_MOUSEMOTION:
            {
                SDL_GetMouseState(&x, &y);
                Point mouseMoveCurrentPoint(x, y);
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnMouseOver)
                {
                    screen.mOnMouseOver(mouseMoveCurrentPoint);
                }
                else
                {
                    /*If objects are stacked on the screen.
                     Then the top most one was the clicked on.
                     It will have the highest mZIndex.
                     Once the objects are sorted in ascending order
                     looking for the objects from the end of the vector
                     would find the top most object clicked on.*/
                    auto iterator = screen.mChildObjects.end();
                    /*iterator now is pointing to end which is not a real object.
                     Hence move it one position to make it point the real end object
                     */
                    --iterator;
                    for(; iterator != screen.mChildObjects.begin(); --iterator)
                    {
                        Rectangle rectangle = (*iterator)->GetRectangle();
                        Point point = rectangle.GetPoint();
                        
                        if(x >= point.GetX() &&
                           x <= (point.GetX() + rectangle.GetWidth()) &&
                           y >= point.GetY() &&
                           y <= (point.GetY() + rectangle.GetHeight()))
                        {
                            if((*iterator)->mOnMouseOver)
                            {
                                (*iterator)->mOnMouseOver(mouseMoveCurrentPoint);
                            }
                            break;
                        }
                    }
                }
            }
            break;
            case SDL_MOUSEBUTTONUP:
            {
                SDL_GetMouseState(&x, &y);
                Point mouseButtonUpPoint(x, y);
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnMouseButtonUp)
                {
                    screen.mOnMouseButtonUp(mouseButtonUpPoint);
                }
                else
                {
                    /*If objects are stacked on the screen.
                     Then the top most one was the clicked on.
                     It will have the highest mZIndex.
                     Once the objects are sorted in ascending order
                     looking for the objects from the end of the vector
                     would find the top most object clicked on.*/
                    auto iterator = screen.mChildObjects.end();
                    /*iterator now is pointing to end which is not a real object.
                     Hence move it one position to make it point the real end object
                     */
                    --iterator;
                    for(; iterator != screen.mChildObjects.begin(); --iterator)
                    {
                        Rectangle rectangle = (*iterator)->GetRectangle();
                        Point point = rectangle.GetPoint();
                        
                        if(x >= point.GetX() &&
                           x <= (point.GetX() + rectangle.GetWidth()) &&
                           y >= point.GetY() &&
                           y <= (point.GetY() + rectangle.GetHeight()))
                        {
                            if((*iterator)->mOnMouseButtonUp)
                            {
                                (*iterator)->mOnMouseButtonUp(mouseButtonUpPoint);
                            }
                            break;
                        }
                    }
                }
            }
            break;
        }
        /*Process the detected Gestures*/
        GestureDetector::Gesture gesture = mGestureDetector.Input(e);
        switch(gesture.mAction)
        {
            case GestureDetector::Gesture::Action::None:
            case GestureDetector::Gesture::Action::Quit:
            break;
            case GestureDetector::Gesture::Action::Click:
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnMouseButtonClick)
                {
                    Point clickedPoint(gesture.mCurrentPoint.GetX(), gesture.mCurrentPoint.GetY());
                    screen.mOnMouseButtonClick(clickedPoint);
                }
                else
                {
                    /*If objects are stacked on the screen.
                     Then the top most one was the clicked on.
                     It will have the highest mZIndex.
                     Once the objects are sorted in ascending order
                     looking for the objects from the end of the vector
                     would find the top most object clicked on.*/
                    auto iterator = screen.mChildObjects.end();
                    /*iterator now is pointing to end which is not a real object.
                     Hence move it one position to make it point the real end object
                     */
                    --iterator;
                    for(; iterator != screen.mChildObjects.begin(); --iterator)
                    {
                        Rectangle rectangle = (*iterator)->GetRectangle();
                        Point point = rectangle.GetPoint();
                        
                        if(gesture.mCurrentPoint.GetX() >= point.GetX() &&
                           gesture.mCurrentPoint.GetX() <= (point.GetX() + rectangle.GetWidth()) &&
                           gesture.mCurrentPoint.GetY() >= point.GetY() &&
                           gesture.mCurrentPoint.GetY() <= (point.GetY() + rectangle.GetHeight()))
                        {
                            if((*iterator)->mOnMouseButtonClick)
                            {
                                Point clickedPoint(gesture.mCurrentPoint.GetX(), gesture.mCurrentPoint.GetY());
                                (*iterator)->mOnMouseButtonClick(clickedPoint);
                            }
                            break;
                        }
                    }
                }
                break;
            case GestureDetector::Gesture::Action::Drag:
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnDrag)
                {
                    screen.mOnDrag(gesture.mStartingPoint, gesture.mCurrentPoint);
                }
                //                else
                //                {
                //                    /*If objects are stacked on the screen.
                //                     Then the top most one was the clicked on.
                //                     It will have the highest mZIndex.
                //                     Once the objects are sorted in ascending order
                //                     looking for the objects from the end of the vector
                //                     would find the top most object clicked on.*/
                //                    for(auto iterator = screen.mChildObjects.end(); iterator != screen.mChildObjects.begin(); --iterator)
                //                    {
                //                        Rectangle rectangle = *iterator->GetRectangle();
                //                        Point point = rectangle.GetPoint();
                //
                //                        if(gesture.mStartingPoint.GetX() >= point.GetX() &&
                //                           gesture.mStartingPoint.GetX() <= (point.GetX() + rectangle.GetWidth()) &&
                //                           gesture.mStartingPoint.GetY() >= point.GetY() &&
                //                           gesture.mStartingPoint.GetY() <= (point.GetY() + rectangle.GetHeight()))
                //                        {
                //                            if(*iterator->mOnDrag)
                //                            {
                //                                *iterator->mOnDrag(gesture.mStartingPoint, gesture.mCurrentPoint);
                //                            }
                //                            break;
                //                        }
                //                    }
                //                }
                break;
            case GestureDetector::Gesture::Action::Drop:
                /*Try to send it to the Screen.
                 If we can't send it to the screen, then
                 send it to the objects.
                 We could do it other way. However
                 it would cause huge performance penality.
                 */
                if(screen.mOnDrop)
                {
                    screen.mOnDrop(gesture.mStartingPoint, gesture.mCurrentPoint);
                }
                //                else
                //                {
                //                    /*If objects are stacked on the screen.
                //                     Then the top most one was the clicked on.
                //                     It will have the highest mZIndex.
                //                     Once the objects are sorted in ascending order
                //                     looking for the objects from the end of the vector
                //                     would find the top most object clicked on.*/
                //                    for(auto iterator = screen.mChildObjects.end(); iterator != screen.mChildObjects.begin(); --iterator)
                //                    {
                //                        Rectangle rectangle = *iterator->GetRectangle();
                //                        Point point = rectangle.GetPoint();
                //
                //                        if(gesture.mStartingPoint.GetX() >= point.GetX() &&
                //                           gesture.mStartingPoint.GetX() <= (point.GetX() + rectangle.GetWidth()) &&
                //                           gesture.mStartingPoint.GetY() >= point.GetY() &&
                //                           gesture.mStartingPoint.GetY() <= (point.GetY() + rectangle.GetHeight()))
                //                        {
                //                            if(*iterator->mOnDrop)
                //                            {
                //                                *iterator->mOnDrop(gesture.mStartingPoint, gesture.mCurrentPoint);
                //                            }
                //                            break;
                //                        }
                //                    }
                //                }
                break;
        }
    }
}

// std::unique_ptr<Image> Engine::TakeSnapshot()
// {
//     int width = 0;
//     int height = 0;
//     SDL_GetWindowSize(spWindow.get()->GetSDLObject(), &width, &height);
//     std::shared_ptr<Surface> copyOFWindowSurface = std::make_shared<Surface>(width, height);
    
//     Point point(0,0);
//     Rectangle targetRectangle(point, width, height))
    
//     copyOFWindowSurface->Blit(SDL_GetWindowSurface(spWindow.get()->GetSDLObject()), targetRectangle);
//     std::unique_ptr<Image> imageToReturn = std::make_unique<Image>("Screen shot", copyOFWindowSurface, targetRectangle);
//     return imageToReturn;
// }
