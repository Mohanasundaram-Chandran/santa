//
//  GestureDetector.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 13/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef GestureDetector_h
#define GestureDetector_h

#include "../SDL/Point.h"

#include <SDL2/SDL.h>
#include <vector>
using namespace std;

class GestureDetector
{
public:
    class Gesture
    {
    public:
        enum Action
        {
            None,
            Click,
            Drag,
            Drop,
            Quit
        };
        
        Action mAction;
        Point mStartingPoint;
        Point mCurrentPoint;
        Gesture(): mStartingPoint(Point(0,0)), mCurrentPoint(Point(0,0))
        {
            
        }
        ~Gesture()
        {                
        }
    };
private:
    
    Gesture mGesture;
    Gesture mDummy;
    vector<int> mPreviousActions;
    
public:
    GestureDetector();
    Gesture & Input(SDL_Event e) noexcept;
};

#endif /* GestureDetector_h */
