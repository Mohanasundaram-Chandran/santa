//
//  SwapAnimation.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 09/09/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "SwapAnimation.h"
#include "Rectangle.h"

SwapAnimation::SwapAnimation(std::shared_ptr<Object> spObject1, std::shared_ptr<Object> spObject2)
{
    if(spObject1->GetRectangle().GetPoint().GetX() > spObject2->GetRectangle().GetPoint().GetX())
    {
        mMovementPlane = MovementPlane::Horizontal;
        mspIncrementingObject = spObject2;
        mspDecrementingObject = spObject1;
    }
    else if(spObject1->GetRectangle().GetPoint().GetX() < spObject2->GetRectangle().GetPoint().GetX())
    {
        mMovementPlane = MovementPlane::Horizontal;
        mspIncrementingObject = spObject1;
        mspDecrementingObject = spObject2;
    }
    else if(spObject1->GetRectangle().GetPoint().GetY() > spObject2->GetRectangle().GetPoint().GetY())
    {
        mMovementPlane = MovementPlane::Vertical;
        mspIncrementingObject = spObject2;
        mspDecrementingObject = spObject1;
    }
    else if(spObject1->GetRectangle().GetPoint().GetY() < spObject2->GetRectangle().GetPoint().GetY())
    {
        mMovementPlane = MovementPlane::Vertical;
        mspIncrementingObject = spObject1;
        mspDecrementingObject = spObject2;
    }
    mStartingIncrementingRectangle = mspIncrementingObject->GetRectangle();
    mStartingDecrementingRectangle = mspDecrementingObject->GetRectangle();
}

bool SwapAnimation::Move()
{
    if(mStartingIncrementingRectangle == mspDecrementingObject->GetRectangle() &&
       mStartingDecrementingRectangle == mspIncrementingObject->GetRectangle())
    {
        /*Animation over*/
        return false;
    }
    
    switch (mMovementPlane)
    {
        case MovementPlane::Horizontal:
        {
            size_t currentDecrementingX = mspDecrementingObject->GetRectangle().GetPoint().GetX();
            mspDecrementingObject->GetRectangle().GetPoint().SetX(currentDecrementingX - mPixelsToMoveEveryFrame);

            size_t currentIncrementingX = mspIncrementingObject->GetRectangle().GetPoint().GetX();
            mspDecrementingObject->GetRectangle().GetPoint().SetX(currentIncrementingX + mPixelsToMoveEveryFrame);
        }
            break;
        case MovementPlane::Vertical:
        {
            size_t currentDecrementingY = mspDecrementingObject->GetRectangle().GetPoint().GetY();
            mspDecrementingObject->GetRectangle().GetPoint().SetY(currentDecrementingY - mPixelsToMoveEveryFrame);
            
            size_t currentIncrementingY = mspIncrementingObject->GetRectangle().GetPoint().GetY();
            mspDecrementingObject->GetRectangle().GetPoint().SetY(currentIncrementingY + mPixelsToMoveEveryFrame);
        }
            break;
            
        default:
            break;
    }
    return true;
}

void SwapAnimation::SetPixelsToMoveEveryFrame(size_t pixelsToMoveEveryFrame)
{
    mPixelsToMoveEveryFrame = pixelsToMoveEveryFrame;
}

void SwapAnimation::ReStart()
{
}
