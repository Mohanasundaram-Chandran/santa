//
//  SwapAnimation.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 09/09/17.
//  Copyright © 2017 Cube. All rights reserved.
//

/*SwapAnimation class has the ability to find out in which direction the swap should happen based on the object postions.
 Hence just passing the objects would do the job.
 */
#ifndef SwapAnimation_h
#define SwapAnimation_h

#include "Animation.h"

class SwapAnimation: public Animation
{
private:
    enum MovementPlane
    {
        Horizontal,
        Vertical
    };
    
    MovementPlane mMovementPlane;
    std::shared_ptr<Object> mspIncrementingObject;
    std::shared_ptr<Object> mspDecrementingObject;
    size_t mPixelsToMoveEveryFrame;
    Rectangle mStartingIncrementingRectangle;
    Rectangle mStartingDecrementingRectangle;
    size_t mStartingValue = 0;
    size_t mMaxValue = 0;
    size_t
public:
    SwapAnimation(std::shared_ptr<Object> object1, std::shared_ptr<Object> object2);
    void SetPixelsToMoveEveryFrame(size_t pixelsToMoveEveryFrame);
    void Restart();
    ~SwapAnimation();
    };


#endif /* SwapAnimation_h */
