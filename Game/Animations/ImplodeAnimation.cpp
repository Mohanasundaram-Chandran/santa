//
//  ImplodeAnimation.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 16/09/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "ImplodeAnimation.h"

ImplodeAnimation::ImplodeAnimation(vector<std::shared_ptr<Tile>> &tiles, size_t pixelsToMoveEveryFrame)
{
    mTiles = tiles;
    for(const auto tile:mTiles)
    {
        shared_ptr<Object> spObject = tile->GetObject();
        mStartingRectangles.push_back(spObject->GetRectanlge());
        int x = spObject->GetRectanlge().GetPoint().GetX() + (spObject->GetRectanlge().GetWidth()/2);
        int y = spObject->GetRectanlge().GetPoint().GetY() + (spObject->GetRectanlge().GetHeight()/2);
        mTargetRectangles.push_back(Rectangle(Point(x, y), 0 , 0));
    }
    mPixelsToMoveEveryFrame = pixelsToMoveEveryFrame;
}

bool ImplodeAnimation::Move()
{
    bool didMove = false;
    
    for(int i=0; i<mObjects.size(); i++)
    {
        Rectangle nextRectangle(mTiles[i]->GetObject()->GetRectanlge());
        
        if(nextRectangle.GetWidth() == mTargetRectangles[i].GetWidth() &&
           nextRectangle.GetHeight() == mTargetRectangles[i].GetHeight() &&
           nextRectangle.GetX() == mTargetRectangles[i].GetX() &&
           nextRectangle.GetY() == mTargetRectangle[i].GetY())
        {
            continue;
        }
        
        size_t nextWidth = nextRectangle.GetWidth() - mPixelsToMoveEveryFrame;
        if(nextWidth <= mTargetRectangles[i].GetWidth())
        {
            nextRectangle.SetWidth(nextWidth);
            nextRectangle.SetX(nextRectangle.GetX() + (nextWidth/2));
        }
        else
        {
            nextRectangle.SetWidth(mTargetRectangles[i].GetWidth());
            nextRectangle.SetX(mTargetRectangles[i].GetX());
        }
        
        size_t nextHeight = nextRectangle.GetHeight() - mPixelsToMoveEveryFrame;
        if(nextHeight < mTargetRectangles[i].GetHeight())
        {
            nextRectangle.SetHeight(nextHeight);
            nextRectangle.SetY(nextHeight/2);
        }
        else
        {
            nextRectangle.SetHeight(mTargetRectangles[i].GetHeight());
            nextRectangle.SetY(mTargetRectangles[i].GetY());
        }
        
        mTiles[i]->GetObject()->SetRectangle(nextRectangle);
        didMove = true;
    }
    return didMove;
}

void ImplodeAnimation::SetPixelsToMoveEveryFrame(size_t pixelsToMoveEveryFrame)
{
    mPixelsToMoveEveryFrame = pixelsToMoveEveryFrame;
}

void ImplodeAnimation::ReStart()
{
    /*We are not going to loop.
     So we don't need to implement this for this animaation*/
}
