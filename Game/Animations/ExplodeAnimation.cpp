//
//  ExplodeAnimation.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 05/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include <iostream>
#include "ExplodeAnimation.h"
#include "../../Engine/SDL/Rectangle.h"

ExplodeAnimation::ExplodeAnimation(std::shared_ptr<Object> spObject, Point screenAnchor, int pixelsToMoveEveryFrame):
mScreenAnchor(screenAnchor), mspObject(spObject)
{
    SetPixelsToMoveEveryFrame(pixelsToMoveEveryFrame);
}

/*!
 @discussion This function is called by the engine before drawing each frame.
 This is the equalent of BeforeDrawingEveryFrame() in Screen with one small difference.
 
 @remark This function has the responsibility to tell the engine when the animation is over by
 returning false.
 
 @return bool
 false - if the animation is over.
 true  - otherwise.
 
 @code Example Implementation:
 bool Move()
 {
 if(animationIsOver)
 {
 return false;
 }
 else
 {
 return true;
 }
 }
 @endcode
 */
bool ExplodeAnimation::Move()
{
    Rectangle nextRectangle(mspObject->GetRectangle());
    
    if(nextRectangle.GetWidth() == mTargetRectangle.GetWidth() &&
       nextRectangle.GetHeight() == mTargetRectangle.GetHeight() &&
       nextRectangle.GetPoint().GetX() == mTargetRectangle.GetPoint().GetX() &&
       nextRectangle.GetPoint().GetY() == mTargetRectangle.GetPoint().GetY())
    {
        return false;
    }
    
    int nextWidth = nextRectangle.GetWidth() + mPixelsToMoveEveryFrame;
    if(nextWidth <= mTargetRectangle.GetWidth())
    {
        nextRectangle.SetWidth(nextWidth);
        nextRectangle.SetX(nextRectangle.GetPoint().GetX() - (mPixelsToMoveEveryFrame/2));
    }
    else
    {
        nextRectangle.SetWidth(mTargetRectangle.GetWidth());
        nextRectangle.SetX(mTargetRectangle.GetPoint().GetX());
    }
    
    int nextHeight = nextRectangle.GetHeight() + mPixelsToMoveEveryFrame;
    if(nextHeight <= mTargetRectangle.GetHeight())
    {
        nextRectangle.SetHeight(nextHeight);
        nextRectangle.SetY(nextRectangle.GetPoint().GetY() - (mPixelsToMoveEveryFrame/2));
    }
    else
    {
        nextRectangle.SetHeight(mTargetRectangle.GetHeight());
        nextRectangle.SetY(mTargetRectangle.GetY());
    }
    
    mspObject->SetRectangle(nextRectangle);
    return true;
}

/*!
 @discussion This function sets the speed of the animation by specifying the number of pixels to move per frame.
 The rationale behind the even numbers is that the X and Y coordinates are moved half of the pixels passed.
 Taking an odd number unncessarily complicates it. Keep it simple. Take only even numbers.
 
 @param pixelsToMoveEveryFrame number of even pixels to move per frame. If an odd number is passed it will be downsized to it's previous even number. For example passing 3 will become 2. Values less than 2 will be upsized to 2;
 
 */
void ExplodeAnimation::SetPixelsToMoveEveryFrame(int pixelsToMoveEveryFrame)
{
    /*Only even numbers are supported*/
    //Handle less than 2
    pixelsToMoveEveryFrame = pixelsToMoveEveryFrame < 2 ? 2 : pixelsToMoveEveryFrame;
    mPixelsToMoveEveryFrame = pixelsToMoveEveryFrame - (pixelsToMoveEveryFrame%2);
    std::cout<<mPixelsToMoveEveryFrame<<std::endl;
}

/*!
 @discussion This function starts the animation.
 User should call this to start the animation.
 If the animation is set to run in loop by the user then the Engine would call this function every time the animation ends.
 Things to Note:
 The aniamtion should have been added to the AnimationList
 The animation should have been SetActive(true)
 */
void ExplodeAnimation::Start()
{
    /*Starting rectangle will be at (mScreenAnchor.x, mScreenAnchor.y, 0, 0)*/
    /*Target rectangle will be at ((mScreenAnchor.x - object.w), (mScreenAnchor.y - object.h), object.width, object.height)*/
    
    /*Just initialise the rectangles with something*/
    mStartingRectangle = mspObject->GetRectangle();
    mTargetRectangle = mspObject->GetRectangle();
    
    //    mStartingRectangle.SetX(mspObject->GetRectangle().GetWidth()/2);
    //    mStartingRectangle.SetY(mspObject->GetRectangle().GetHeight()/2);
    mStartingRectangle.SetX(mScreenAnchor.GetX());
    mStartingRectangle.SetY(mScreenAnchor.GetY());
    mStartingRectangle.SetWidth(0);
    mStartingRectangle.SetHeight(0);
    
    mTargetRectangle.SetX(mScreenAnchor.GetX() - (mspObject->GetRectangle().GetWidth()/2));
    mTargetRectangle.SetY(mScreenAnchor.GetY() - (mspObject->GetRectangle().GetHeight()/2));
    mTargetRectangle.SetWidth(mspObject->GetRectangle().GetWidth());
    mTargetRectangle.SetHeight(mspObject->GetRectangle().GetHeight());
    
    mspObject->SetRectangle(mStartingRectangle);
}

ExplodeAnimation::~ExplodeAnimation()
{
}
