//
//  ImplodeAnimation.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 16/09/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef ImplodeAnimation_h
#define ImplodeAnimation_h

#include "Animation.h"
#include "Rectangle.h"
#include "Object.h"

#include <vector>
#include <memory>
#include <cstddef>
using namespace std;

class ImplodeAnimation: public Animation
{
private:
    size_t mPixelsToMoveEveryFrame;
    vector<shared_ptr<Tile>> mTiles;
    vector<Rectangle> mStartingRectangles;
    vector<Rectangle> mTargetRectangles;
    size_t mStartingValue = 0;
    size_t mMaxValue = 0;
    size_t
public:
    ImplodeAnimation(vector<shared_ptr<Tile>> &tiles, size_t pixelsToMoveEveryFrame);
    void SetPixelsToMoveEveryFrame(size_t pixelsToMoveEveryFrame)
    void Restart();
    ~ImplodeAnimation();
};

#endif /* ImplodeAnimation_h */
