//
//  ExplodeAnimation.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 05/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef ExplodeAnimation_h
#define ExplodeAnimation_h

#include "../../Engine/UI/Animation.h"
#include "../../Engine/UI/Object.h"
#include "../../Engine/SDL/Point.h"

class ExplodeAnimation: public Animation
{
private:
    std::shared_ptr<Object> mspObject;

    int mPixelsToMoveEveryFrame;
    Rectangle mStartingRectangle;
    Rectangle mTargetRectangle;
    size_t mStartingValue = 0;
    Point mScreenAnchor;
public:
    ExplodeAnimation(std::shared_ptr<Object> object, Point screenAnchor, int pixelsToMoveEveryFrame = 2);
    void SetPixelsToMoveEveryFrame(int pixelsToMoveEveryFrame);
    bool Move() override;
    void Start() override;
    ~ExplodeAnimation();
};

#endif /* ExplodeAnimation_h */
