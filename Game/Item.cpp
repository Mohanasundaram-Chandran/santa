//
//  Item.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 12/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Global.h"
#include "Item.h"
#include <SDL2/SDL.h>

SetObject(std::shared_ptr<Image> spObject)
{
    mspObject = spObject;
}

SetTile(Tile * pTile)
{
    mpTile = pTile;//TODO: get rid of the raw pointer
}
