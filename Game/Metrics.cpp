//
//  Metrics.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Metrics.h"

Metrics *Metrics::mpMetrics = NULL;

Metrics::Metrics()
{
}

Metrics *Metrics::GetInstance()
{
    if(!mpMetrics)
    {
        mpMetrics = new Metrics();
    }
    return mpMetrics;
}

void Metrics::Reset()
{
    mItemsCollected = 0;
    mRelicsEarned = 1;
    mWandsEarned = 0;
    mScore = 0;
}
