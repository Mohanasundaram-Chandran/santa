//
//  main.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//
//  Just an entry point to the App.

#include "../Engine/Kernel/Engine.h"
#include "../Engine/SDL/Window.h"
#include "FTUEScreen.h"
// #include "GameScreen.h"
// #include "GameOverScreen.h"
#include "Global.h"
#include "main.h"

int main(int argc, const char * argv[])
{
    Engine engine(SCREEN_WIDTH, SCREEN_HEIGHT, GAME_NAME);
    engine.SetTargetFrameRate(60);

    FTUEScreen ftueScreen;
    engine.DisplayScreen(ftueScreen);
    // while(true)
    // {
    //     GameScreen newGameScreen();
    //     engine.GetWindow()->DisplayScreen(newGameScreen);
    //     GameOverScreen newGameOverScreen();
    //     engine.GetWindow()->DisplayScreen(newGameOverScreen);
    // }
    
    return 0;
}
