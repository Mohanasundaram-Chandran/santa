//
//  GestureDetector.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 13/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#ifndef GestureDetector_h
#define GestureDetector_h

#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
using namespace std;

class GestureDetector
{
public:
    class Gesture
    {
    public:
        enum Event
        {
            MouseButtonDown,
            MouseButtonUp,
            MouseMove,
        };

        enum GestureName
        {
            None,
            Click,
            Drag,
            Drop,
            Quit
        };
        
        GestureName mGestureName;
        int mStartingX;
        int mStartingY;
        int mCurrentX;
        int mCurrentY;
    };
private:
    
    Gesture mGesture;
    Gesture mDummy;
    vector<int> mPreviousActions;
    
public:
    GestureDetector();
    Gesture & Input(Event event);
    
};

#endif /* GestureDetector_h */
