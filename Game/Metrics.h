//
//  Metrics.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef Metrics_h
#define Metrics_h

#include <iostream>
using namespace std;

class Metrics
{
private:
    static Metrics *mpMetrics;
    Metrics();
public:
    int mItemsCollected = 0;
    int mRelicsEarned = 1;
    int mWandsEarned = 0;
    long mScore = 0;
    
    static Metrics *GetInstance();
    void Reset();
};
#endif /* Metrics_h */
