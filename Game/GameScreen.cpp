//
//  GameScreen.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "main.h"
#include "GameScreen.h"
#include "Metrics.h"

#include <ctime>

/*
 This screen class is designed in such a way that it is created once
 and can be loaded multiple times whenever a new game starts
 */

/*
 Loads sounds
 Creates scoreboard objects
 Create go animation and add it to the animation list
 set event handlers
 */
GameScreen::GameScreen()
{
    /*Load Sounds*/
    Engine::GetAudioManager().LoadSoundEffect(MATCH_SOUND_EFFECT);
    Engine::GetAudioManager().LoadSoundEffect(DROP_SOUND_EFFECT);
    Engine::GetAudioManager().LoadSoundEffect(LIGHTNING_SOUND_EFFECT);
    Engine::GetAudioManager().LoadSoundEffect(POP_SOUND_EFFECT);
    Engine::GetAudioManager().LoadSoundEffect(MOVE_SOUND_EFFECT);
    /*Loading sounds done*/
    
    /*Create objects*/
    /*Scoreboard objects*/
    //TODO: Scoreboard objects should have one zIndex more than background
    mspScoreBoardBackground = std::shared_ptr<Image>("Scoreboard Background", Colour(SCOREBOARD_BACKGROUND_COLOUR_RGBA), Rectangle(Point(0, BOARD_HEIGHT), SCORE_BOARD_WIDTH, SCORE_BOARD_HEIGHT));
    
    /*First Row*/
    mspBasket = std::make_shared<Image>(COLLECTED_IMAGE, COLLECTED_IMAGE, Rectangle(Point(OBJECT_WIDTH * 0,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplier = std::make_shared<Image>(SPEED_IMAGE, SPEED_IMAGE, Rectangle(Point(OBJECT_WIDTH * 2,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspScore = std::make_shared<Image>(SCORE_IMAGE, SCORE_IMAGE, Rectangle(Point(OBJECT_WIDTH * 4,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));

    if(AudioManager::GetInstance()->MusicState())
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_ON_ICON, MUSIC_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    else
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_OFF_ICON, MUSIC_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    mspMusicIcon.mOnClick = MusicIconOnClickHandler;
    
    mspTimer = std::make_shared<Image>(TIMER_IMAGE, TIMER_IMAGE, Rectangle(Point(OBJECT_WIDTH * 7,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));

    /*Second Row*/
    mspCollectedCount = std::make_shared<TextBox>("Collected Count", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 0,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplicationSymbol = std::make_shared<TextBox>("Multiplication Symbol", "x", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 1,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplierValue = std::make_shared<TextBox>("Multiplier Value", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 2,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspEqualToSymbol = std::make_shared<TextBox>("Equal To Symbol", "=", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 3,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspScoreValue = std::make_shared<TextBox>("Score Value", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 4,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));

    if(AudioManager::GetInstance()->SoundEffectsState())
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_ON_ICON, SOUND_EFFECTS_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    else
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_OFF_ICON, SOUND_EFFECTS_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    mspSoundEffectsIcon.mOnClick = SoundEffectsIconOnClickHandler;
    
    mspTimerValue = std::make_shared<TextBox>("Timer Value", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 7,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspLoadingText = std::make_shared<TextBox>("Loading Text", "Loading Begins", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(0, 0), BOARD_WIDTH, BOARD_HEIGHT));

    /*Scoreboard objects done*/
    
    /* Add objects to the display list*/
    AddObjectToDisplayList(mspScoreBoardBackground);
    AddObjectToDisplayList(mspBasket);
    AddObjectToDisplayList(mspMultiplier);
    AddObjectToDisplayList(mspScore);
    AddObjectToDisplayList(mspMusicIcon);
    AddObjectToDisplayList(mspTimer);
    AddObjectToDisplayList(mspCollectedCount);
    AddObjectToDisplayList(mspMultiplicationSymbol);
    AddObjectToDisplayList(mspMultiplierValue);
    AddObjectToDisplayList(mspEqualToSymbol);
    AddObjectToDisplayList(mspScoreValue);
    AddObjectToDisplayList(mspSoundEffectsIcon);
    AddObjectToDisplayList(mspTimerValue);
    AddObjectToDisplayList(mspLoadingText);
    
    /*Display the board*/
    SetBackground(BOARD_BACKGROUND_COLOUR_RGBA);

    mspGoText = std::make_shared<TextBox>("Go", "Go", GAMEBOARD_FONT_COLOUR_RGBA, Rectangle(Point(mRectangle.GetPoint().GetX(), mRectangle.GetPoint().GetY()), mRectangle.GetWidth(), mRectangle.GetHeight()));
    AddObjectToDisplayList(mspGoText);

    std::shared_ptr<ExplodeAnimation> spExplodeAnimation(mspGoText, 3);
    AddAnimationToList(spExplodeAnimation);
    
    /*Event Handlers*/
    mOnMouseButtonClick = MouseButtonClickHandler;
    mOnDrag = DragHandler;
    mOnDrop = DropHandler;
}

/*
 Create game board objects by randomly selecting the object type and assign them to the tiles
 Call DoMagic once and make sure the board is good enough to start the game
 Add all the objects assigned to the tiles in Display List
 Create Explode animations for each object and assign them to the animation list
 Resume music
 Start the metrics fresh
 */
void OnLoad()
{
    /*mIsBoarding is used by functions like Magic Wand to see if the board is loading.
     When the board is loading, Magic Wand is not granted.
     Also some functions use this flag to decide whether to display animation or not.
     If the board is loading it does not play animataion
     */
    mIsBoardLoading = true;

    /*Create objects used in game*/
    /*Just a list of item names that are being used in the game*/
    mItemNames.push_back(string(OBJECT1_NAME));//For simplicity file name is synonimous to object name
    mItemNames.push_back(string(OBJECT2_NAME));
    mItemNames.push_back(string(OBJECT3_NAME));
    mItemNames.push_back(string(OBJECT4_NAME));
    mItemNames.push_back(string(OBJECT5_NAME));
    
    /*Make the board*/
    int previousIndex = 0;
    srand ((unsigned int)time(NULL));
    for(int i=0;i<ROWS;i++)
    {
        for(int j=0;j<COLUMNS;j++)
        {
            int index = 0;
            do
            {
                index = rand()%5;
            }while(index == previousIndex);
            /*Prepare the tile*/
            mTiles[i][j].SetPosition(Point(i*BOARD_WIDTH/COLUMNS,j*BOARD_HEIGHT/ROWS));
            
            /*Prepare the object to drop in the tile*/
            string imageName = mItemNames[index];
            string imagePath(IMAGES_PATH);
            path.append(mItemNames[index]->c_str());
            Rectangle imageRectangle(Point(0,0),OBJECT_WIDTH, OBJECT_HEIGHT);
            std::shared_ptr<Image> spImage = std::make_shared<Image>(imageName, imagePath, imageRectangle);
            
            /*Drop the object in the tile*/
            mTiles[i][j].DropImage(spImage);
            
            previousIndex = index;
        }
    }
    
    DoMagic();//Just iron out wrinkles.
    /*Game objects created*/

    mpLastMove = new Move();//TODO: Is this in the right place?

    for(int i=0;i<ROWS;i++)
    {
        for(int j=0;j<COLUMNS;j++)
        {
            AddObjectToDisplayList(mTiles[i][j].GetImage());
            mspObjects.push_back(mTiles[i][j].GetImage());
        }
    }

    /*
     1. Constructor creats objects chosing the type of the object in random order and assign it to the tiles.
     2. OnLoad function picks objects in the tiles randomly and create explode animations with increasing speed and add them to the animation list.
     3. In the function BeforeDrawingEveryFrame() iterate through the vector AnimationsList and activate them one per frame or as it fits.
     4. When each animation ends play pop sound.
     */
    
    srand ((unsigned int)time(NULL));
    int i = 0;
    int animationSpeed = 1;
    do
    {
        int index = 0;
        index = rand()%mspObjects.size();
        std::shared_ptr<ExplodeAnimation> spExplodeAnimation(mspObjects[index], animationSpeed);
        spExplodeAnimation.mOnEnd = ExplodeAnimationOnEndHandler;
        mspObjectAnimations.push_back(spExplodeAnimation);
        AddAnimationToList(spExplodeAnimation);
        mspObjects.erase(mspObjects.begin() + index);
        if (i<8)
        {
            animationSpeed = 1;
        }
        else if(i<16)
        {
            animationSpeed = 2;
        }
        else if(i<32)
        {
            animationSpeed = 4;
        }
        else if(i<64)
        {
            animationSpeed = 8;
        }
        i++;
    }while(mspObjects.size());
    
    mpAudioManager->ResumeMusic();
    //Start clean
    Metrics::GetInstance()->Reset();//TODO:Add a map in the Engine to allow the games to store and retrieve the data. It could be made persistent. Store in hard disk and retrive as an object dump.
    mIsBoardLoading = false;
}

void BeforeDrawingEveryFrame()
{
    /*Irrespective of whether there was a swap of objects or not
     keep refresing the scoreboard. Because the scoreboard has timer.
     */
    SetScoreBoardValues();
    
    /*mIsSwapRequestedByPlayer is set to true by the MouseButtonClick/Drag Handlers,
     indicating that a swap has happened and requesting the BeforeDrawingEveryFrame to 
     process the swap.
     */
    /*TODO: Why did I put it in while loop instead of if.
     Looking at the Flow diagram I added last week in MindMapLite looks like the process is done
     in multiple frames as it involves animations and everything goes in sequence.
     I guess at the end the plan is to set mIsSwapRequestedByPlayer to false.
     */
    if(mIsSwapRequestedByPlayer &&
       !mIfSwapInProgress)
    {
        mIfSwapInProgress = true;
        ProcessSwap();
    }
}

void GameScreen::SetScoreBoardValues()
{
    mspCollectedCount->Set(std::to_string(Metrics::GetInstance()->mItemsCollected));
    mspMultiplierValue->Set(std::to_string(Metrics::GetInstance()->mRelicsEarned));
    mspScoreValue->Set(std::to_string(Metrics::GetInstance()->mItemsCollected * Metrics::GetInstance()->mRelicsEarned));
    
    double elapsedSeconds = GetElapsedSeconds();
    if(elapsedSeconds > 0 &&
       elapsedSeconds < GAME_TIME_IN_SECONDS)
    {
        mspTimerValue->Set(std::to_string((int)(GAME_TIME_IN_SECONDS - elapsedSeconds)));
    }
    else
    {
        mspTimerValue->Update(std::to_string(0));
    }
}

void ProcessSwap()
{
    /*
     * 1. If Adjacent Swap
     * 2. Trigger Swap Animation
     */
    if(IsAdjacentSwap())
    {
        /*Triggers swap animation and at the end does the real swapping*/
        SwapObjects(mpSourceTile, mpTargetTile, true);
        
        
    }
    FinishUpProcessSwap();
}

void FinishUpProcessSwap()
{
    mpSourceTile = NULL;
    mpTargetTile = NULL;
    mIsSwapRequestedByPlayer = false;
}

/*This functions validates whether the swap initiated by the player is valid*/
bool GameScreen::IsAdjacentSwap()
{
    int iSourceIndex = mpSourceTile->mPosition.x/TILE_WIDTH;
    int jSourceIndex = mpSourceTile->mPosition.y/TILE_HEIGHT;
    int iTargetIndex = mpTargetTile->mPosition.x/TILE_WIDTH;
    int jTargetIndex = mpTargetTile->mPosition.y/TILE_HEIGHT;
    
    if(((iSourceIndex - 1) == iTargetIndex && jSourceIndex == jTargetIndex) ||
       ((iSourceIndex + 1) == iTargetIndex && jSourceIndex == jTargetIndex) ||
       (iSourceIndex == iTargetIndex && (jSourceIndex - 1) == jTargetIndex) ||
       (iSourceIndex == iTargetIndex && (jSourceIndex + 1) == jTargetIndex))
    {
        return true;
    }
    
    return false;
}

void GameScreen::SwapObjects(Tile * pSourceTile, Tile * pTargetTile, bool shouldSwapAnimate)
{
    /*
     SwapObjects are also used by PullDown function. During that time we do not want swap animation.
     Hence PullDown function uses shouldSwapAnimation to indicate that a swap animation is not needed
     */
    if(shouldSwapAnimate)
    {
        /*Initiate the Swap animation.
         At the end of swap animation set
         the source and target tile to null
         */
        std::shared_ptr<Animation> swapAnimation = std::make_shared<SwapAnimation>(pSourceTile->GetObject(), pTargetTile->GetObject());
        AddAnimationToList(swapAnimation);
        swapAnimation->SetPixelsToMovePerFrame(SWAP_ANIMATION_SPEED);
        swapAnimation->OnEnd = SwapAnimationOnEndHandler;
        swapAnimation->Start();
    }
    else
    {
        SwapAnimationOnEndHandler();
    }
}

void GameScreen::SwapBackObjects(Tile * pSourceTile, Tile * pTargetTile, bool shouldSwapAnimate)
{
    /*SwapObjects are also used by PullDown function. During that time we do not want swap animation.*/
    if(shouldSwapAnimate)
    {
        /*Initiate the Swap animation.
         At the end of swap animation set
         the source and target tile to null
         */
        std::shared_ptr<Animation> swapAnimation = std::make_shared<SwapAnimation>(pSourceTile->GetObject(), pTargetTile->GetObject());
        AddAnimationToList(swapAnimation);
        swapAnimation->SetPixelsToMovePerFrame(SWAP_ANIMATION_SPEED);
        swapAnimation->OnEnd = SwapBackAnimationOnEndHandler;
        swapAnimation->Start();
    }
    else
    {
        SwapBackAnimationOnEndHandler();
    }
}

void SwapBackAnimationOnEndHandler()
{
    Item * pItem = pSourceTile->GetObject();
    pSourceTile->DropObject(pTargetTile->GetObject());
    pTargetTile->DropObject(pItem);
    FinishUpProcessSwap();
}

void SwapAnimationOnEndHandler()
{
    Item * pItem = pSourceTile->GetObject();
    pSourceTile->DropObject(pTargetTile->GetObject());
    pTargetTile->DropObject(pItem);
    
    /*If one of the moved objects is magic wand
     then it deletes all the target objects in the board.
     Handle that first.
     */
    mIsMatchSoundEffectPlayed = true;
    if(IsMagicWandUsed())
    {
        HandleMagicWand(mpSourceTile, mpTargetTile);
    }
    else
    {
        if(IsEligibleForMagicWand(mpSourceTile) ||
           IsEligibleForMagicWand(mpTargetTile))
        {
            mpLastMove->mEarnedMagicWand = true;
        }
        
        bool swapBack = true;
        mIsMatchSoundEffectPlayed = false;
        //TODO: Whis is DoMagic() called twice
        if(DoMagic())
        {
            swapBack = false;
        }
        if(DoMagic())
        {
            swapBack = false;
        }
        if(swapBack)
        {
            SwapBackObjects(mpSourceTile, mpTargetTile, true);
        }
        else
        {
            ShowEndOfMoveAnimations();
        }
        mIsMatchSoundEffectPlayed = false;
    }
}

bool IsMagicWandUsed()
{
    if(0 == mpSourceTile->GetObject()->mName.compare(WAND_NAME))
    {
        mpLastMove->mMagicWandUsed = true;
    }
    else if (0 == mpTargetTile->GetObject()->mName.compare(WAND_NAME))
    {
        mpLastMove->mMagicWandUsed = true;
    }
    return mpLastMove->mMagicWandUsed;
}

//TODO: Make sourceTile and targetTile as shared_ptrs
void GameScreen::HandleMagicWand(Tile *pSourceTile, Tile *pTargetTile)
{
    if(mIsBoardLoading)
    {
        return;
    }
    
    Tile *pBombTile = NULL;
    string itemNameBombed;
    if(0 == mpSourceTile->GetObject()->mName.compare(WAND_NAME))
    {
        mpLastMove->mMagicWandUsed = true;
        pBombTile = mpSourceTile;
        itemNameBombed = mpTargetTile->GetObject()->mName;
    }
    else if (0 == mpTargetTile->GetObject()->mName.compare(WAND_NAME))
    {
        mpLastMove->mMagicWandUsed = true;
        pBombTile = mpTargetTile;
        itemNameBombed = mpSourceTile->GetObject()->mName;
    }
    
    vector<shared_ptr<Tile>> mBombedTiles;
    if(mpLastMove->mMagicWandUsed)
    {
        mBombedTiles.push_back(pBombTile->GetObject());
        for(int i=0;i<ROWS;i++)
        {
            for(int j=0;j<COLUMNS;j++)
            {
                if(mTiles[i][j].GetObject())
                {
                    if(mTiles[i][j].GetObject()->mName.compare(itemNameBombed) == 0)
                    {
                        if(mTiles[i][j].GetObject())
                        {
                            Metrics::GetInstance()->mItemsCollected++;
                            mBombedTiles.push_back(mTiles[i][j].GetObject());
                        }
                    }
                }
            }
        }
        
        mpAudioManager->PlaySoundEffect(LIGHTNING_SOUND_EFFECT, 1);
        
        shared_ptr<ImplodeAnimation> spImplodeAnimation = make_shared<ImplodeAnimation>(mBombedTiles, IMPLODE_ANIMATION_SPEED);
        spImplodeAnimation->OnEnd = ImplodeAnimationOnEndHandler;
        spImplodeAnimation->setActive(true);
        spImplodeAnimation->Start();
    }
}

void ImplodeAnimationOnEndHandler()
{
    for(auto tile:mTiles)
    {
        tile->DeleteObject();
        mpLastTileEmptied = tile;
        mpLastMove->mItemsCollected++;
    }
    
    mDoesMagicWandExists = false;
}

//
///*Draw the game board*/
//void GameScreen::DrawBoard(int delay)
//{
//    if(mIsBoardLoading)
//    {
//        return;
//    }
//    SDL_FillRect(SDL_GetWindowSurface(mpWindow), &mPosition, BOARD_BACKGROUND_COLOUR);
//    for(int i=0;i<ROWS;i++)
//    {
//        for(int j=0;j<COLUMNS;j++)
//        {
//            if(mTiles[i][j].GetObject())
//            {
//                SDL_BlitScaled(mTiles[i][j].GetObject()->mpImage, NULL, SDL_GetWindowSurface(mpWindow), &mTiles[i][j].GetObject()->mPosition);
//            }
//        }
//    }
//    
//    SDL_UpdateWindowSurface(mpWindow);
//    SDL_Delay(delay);
//    SDL_Delay(0);
//}

void ExplodeAnimationOnEndHandler()
{
    mpAudioManager->PlaySoundEffect(POP_SOUND_EFFECT, 1);
}

/*Recursively Matches objects, Deletes Objects and Collapse the board
 till the board is ready for the player to make the next move*/
bool GameScreen::DoMagic()
{
    bool didMagic = false;
    int previousIndex = 0;
    while(true)
    {
        DeleteMatchingObjectsAndCollapseRecursively();
        bool didFill = false;
        srand ((unsigned int)time(NULL));
        for(int i=0;i<ROWS;i++)
        {
            for(int j=0;j<COLUMNS;j++)
            {
                if(!mTiles[i][j].GetObject())
                {
                    int index = 0;
                    do
                    {
                        index = rand()%5;
                    }while(index == previousIndex);
                    string imagePath(IMAGES_PATH);
                    string imageName;
                    if(mpLastMove->mEarnedMagicWand &&
                       !mDoesMagicWandExists)
                    {
                        imagePath.append(WAND_NAME);
                        mDoesMagicWandExists = true;
                        imageName = WAND_NAME;
                    }
                    else
                    {
                        imagePath.append(mItemNames[index]->c_str());
                        imageName = mItemNames[index]->c_str();
                    }
                    Rectangle imageRectangle(Point(0,0),OBJECT_WIDTH, OBJECT_HEIGHT);
                    std::shared_ptr<Image> spImage = std::make_shared<Image>(imageName, imagePath, imageRectangle);
                    AddObjectToDisplayList(spImage);
                    mTiles[i][j].DropObject(spImage);
                    previousIndex = index;
                    didFill = true;
                    didMagic = true;
                    break;
                }
            }
            if(didFill)
            {
                break;
            }
        }
        if(!didFill)
        {
            break;
        }
    }
    
    return didMagic;
}

void GameScreen::Collapse()
{
    for(int i=0;i<ROWS;i++)
    {
        for(int j=0;j<COLUMNS;j++)
        {
            PullDown(&mTiles[i][j]);
        }
    }
}

/*Called by do magic to delete objects and collapse the board*/
bool GameScreen::DeleteMatchingObjectsAndCollapseRecursively()
{
    bool didClearObjects = false;
    
    do
    {
        didClearObjects = false;
        Collapse();
        
        for(int i=0;i<ROWS;i++)
        {
            for(int j=0;j<COLUMNS;j++)
            {
                if(DeleteMatchingObjects(&mTiles[i][j]))
                {
                    didClearObjects = true;
                }
            }
        }
    }while(didClearObjects);
    
    return didClearObjects;
}

/*For a given empty tile it goes recursively and pulls down all the coloured objects
 in the tiles above.*/
bool GameScreen::PullDown(Tile *pTile)
{
    bool didCollapse = false;
    int iIndex = pTile->mPosition.x/TILE_WIDTH;
    int jIndex = pTile->mPosition.y/TILE_HEIGHT;
    
    int jTargetIndex = jIndex;
    while(jTargetIndex >= 0)
    {
        Tile * pTargetTile = NULL;
        Tile * pSourceTile = NULL;
        if(!mTiles[iIndex][jTargetIndex].GetObject())
        {
            pTargetTile = &mTiles[iIndex][jTargetIndex];
            int jSourceIndex = jTargetIndex - 1;
            while(jSourceIndex >= 0)
            {
                if(mTiles[iIndex][jSourceIndex].GetObject())
                {
                    pSourceTile = &mTiles[iIndex][jSourceIndex];
                    break;
                }
                jSourceIndex--;
            }
        }
        if(pTargetTile &&
           pSourceTile)
        {
            SwapObjects(pSourceTile, pTargetTile, false);
            didCollapse = true;
        }
        jTargetIndex--;
    }
    if(didCollapse)
    {
        if(!mIsBoardLoading)
        {
            mpAudioManager->PlaySoundEffect(DROP_SOUND_EFFECT, 1);
        }
        
//        DrawBoard(0);
    }
    return didCollapse;
}


void GameScreen::DoHorizontalSearch(int iIndex, int jIndex, Tile * pTile, vector<Tile *> &horizontalTilesToEmpty)
{
    int i = iIndex + 1;
    while(i < ROWS)
    {
        Tile *pTileToCompare = &mTiles[i++][jIndex];
        if(pTile->GetObject() &&
           pTileToCompare->GetObject() &&
           (0 == pTile->GetObject()->mName.compare(pTileToCompare->GetObject()->mName)))
        {
            horizontalTilesToEmpty.push_back(pTileToCompare);
        }
        else
        {
            break;
        }
    }
    
    i = iIndex - 1;
    while(0 <= i)
    {
        Tile *pTileToCompare = &mTiles[i--][jIndex];
        if(pTile->GetObject() &&
           pTileToCompare->GetObject() &&
           (0 == pTile->GetObject()->mName.compare(pTileToCompare->GetObject()->mName)))
        {
            horizontalTilesToEmpty.push_back(pTileToCompare);
        }
        else
        {
            break;
        }
    }
}

void GameScreen::DoVerticalSearch(int iIndex, int jIndex, Tile * pTile, vector<Tile *> &verticalTilesToEmpty)
{
    int j = jIndex + 1;
    while(j < COLUMNS)
    {
        Tile *pTileToCompare = &mTiles[iIndex][j++];
        if(pTile->GetObject() &&
           pTileToCompare->GetObject() &&
           (0 == pTile->GetObject()->mName.compare(pTileToCompare->GetObject()->mName)))
        {
            verticalTilesToEmpty.push_back(pTileToCompare);
        }
        else
        {
            break;
        }
    }
    
    j = jIndex - 1;
    while(0 <= j)
    {
        Tile *pTileToCompare = &mTiles[iIndex][j--];
        if(pTile->GetObject() &&
           pTileToCompare->GetObject() &&
           (0 == pTile->GetObject()->mName.compare(pTileToCompare->GetObject()->mName)))
        {
            verticalTilesToEmpty.push_back(pTileToCompare);
        }
        else
        {
            break;
        }
    }
}


/*Delete the matching objects and pull down the objects above by causing board collapse*/
bool GameScreen::DeleteMatchingObjects(Tile * pTile)
{
    if(!pTile->GetObject())
    {
        return false;
    }
    
    int iIndex = pTile->mPosition.x/TILE_WIDTH;
    int jIndex = pTile->mPosition.y/TILE_HEIGHT;
    
    vector<Tile *> horizontalTilesToEmpty;
    horizontalTilesToEmpty.push_back(pTile);
    
    vector<Tile *> verticalTilesToEmpty;
    
    DoHorizontalSearch(iIndex, jIndex, pTile, horizontalTilesToEmpty);
    
    if(horizontalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: horizontalTilesToEmpty)
        {
            vector<Tile *> tempVerticalTilesToEmpty;
            int iTempIndex = pTempTile->mPosition.x/TILE_WIDTH;
            int jTempIndex = pTempTile->mPosition.y/TILE_HEIGHT;
            tempVerticalTilesToEmpty.push_back(pTempTile);
            DoVerticalSearch(iTempIndex, jTempIndex, pTempTile, tempVerticalTilesToEmpty);
            if(tempVerticalTilesToEmpty.size() <= 2)
            {
                tempVerticalTilesToEmpty.clear();
            }
            for(Tile *pTempTile: tempVerticalTilesToEmpty)
            {
                verticalTilesToEmpty.push_back(pTempTile);
            }
        }
    }
    else
    {
        horizontalTilesToEmpty.clear();
        verticalTilesToEmpty.push_back(pTile);
        DoVerticalSearch(iIndex, jIndex, pTile, verticalTilesToEmpty);
        if(verticalTilesToEmpty.size() > 2)
        {
            for(Tile * pTempTile: verticalTilesToEmpty)
            {
                vector<Tile *> tempHorizontalTilesToEmpty;
                int iTempIndex = pTempTile->mPosition.x/TILE_WIDTH;
                int jTempIndex = pTempTile->mPosition.y/TILE_HEIGHT;
                tempHorizontalTilesToEmpty.push_back(pTempTile);
                DoHorizontalSearch(iTempIndex, jTempIndex, pTempTile, tempHorizontalTilesToEmpty);
                if(tempHorizontalTilesToEmpty.size() <= 2)
                {
                    tempHorizontalTilesToEmpty.clear();
                }
                for(Tile *pTempTile: tempHorizontalTilesToEmpty)
                {
                    horizontalTilesToEmpty.push_back(pTempTile);
                }
            }
        }
    }
    
    bool isDeleted = false;
    if(horizontalTilesToEmpty.size() > 2 ||
       verticalTilesToEmpty.size() > 2)
    {
        if(!mIsBoardLoading &&
           !mpLastMove->mMagicWandUsed &&
           !mIsMatchSoundEffectPlayed)
        {
            mpAudioManager->PlaySoundEffect(MATCH_SOUND_EFFECT, 1);
            mIsMatchSoundEffectPlayed = true;
        }
//        DrawBoard(250);//Let the user see the match for 1/4th of a second
    }
    
    
    vector<Object *> items;

    if(horizontalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: horizontalTilesToEmpty)
        {
            if(std::find(items.begin(), items.end(), pTempTile->GetObject()) == items.end())
            {
                items.push_back(pTempTile->GetObject());
            }
        }
    }

    if(verticalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: verticalTilesToEmpty)
        {
            if(std::find(items.begin(), items.end(), pTempTile->GetObject()) == items.end())
            {
                items.push_back(pTempTile->GetObject());
            }
        }
    }
    
    Metrics::GetInstance()->mItemsCollected += items.size();
    mpLastMove->mItemsCollected += items.size();

    if(!mIsBoardLoading &&
       items.size() >= 3)
    {
        ImplodeAnimation(items, IMPLODE_ANIMATION_SPEED, BOARD_BACKGROUND_COLOUR);//Implode the objects
        PulseAnimation(mpBasket, PULSE_ANIMATION_SPEED);//Pulsate the basket
        SetScoreBoardValues();//Draw the score.
    }
    
    if(horizontalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: horizontalTilesToEmpty)
        {
            if(pTempTile->GetObject())
            {
                pTempTile->DeleteObject();
                mpLastTileEmptied = pTempTile;
            }
            isDeleted = true;
        }
    }
    
    if(verticalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: verticalTilesToEmpty)
        {
            if(pTempTile->GetObject())
            {
                pTempTile->DeleteObject();
                mpLastTileEmptied = pTempTile;
            }
            isDeleted = true;
        }
    }
    
    return isDeleted;
}

/*Delete the matching objects and pull down the objects above by causing board collapse*/
bool GameScreen::IsEligibleForMagicWand(Tile * pTile)
{
    if(!pTile->GetObject())
    {
        return false;
    }
    
    int iIndex = pTile->mPosition.x/TILE_WIDTH;
    int jIndex = pTile->mPosition.y/TILE_HEIGHT;
    
    vector<Tile *> horizontalTilesToEmpty;
    horizontalTilesToEmpty.push_back(pTile);
    
    vector<Tile *> verticalTilesToEmpty;
    
    DoHorizontalSearch(iIndex, jIndex, pTile, horizontalTilesToEmpty);
    
    if(horizontalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: horizontalTilesToEmpty)
        {
            vector<Tile *> tempVerticalTilesToEmpty;
            int iTempIndex = pTempTile->mPosition.x/TILE_WIDTH;
            int jTempIndex = pTempTile->mPosition.y/TILE_HEIGHT;
            tempVerticalTilesToEmpty.push_back(pTempTile);
            DoVerticalSearch(iTempIndex, jTempIndex, pTempTile, tempVerticalTilesToEmpty);
            if(tempVerticalTilesToEmpty.size() <= 2)
            {
                tempVerticalTilesToEmpty.clear();
            }
            for(Tile *pTempTile: tempVerticalTilesToEmpty)
            {
                verticalTilesToEmpty.push_back(pTempTile);
            }
        }
    }
    else
    {
        horizontalTilesToEmpty.clear();
        verticalTilesToEmpty.push_back(pTile);
        DoVerticalSearch(iIndex, jIndex, pTile, verticalTilesToEmpty);
        if(verticalTilesToEmpty.size() > 2)
        {
            for(Tile * pTempTile: verticalTilesToEmpty)
            {
                vector<Tile *> tempHorizontalTilesToEmpty;
                int iTempIndex = pTempTile->mPosition.x/TILE_WIDTH;
                int jTempIndex = pTempTile->mPosition.y/TILE_HEIGHT;
                tempHorizontalTilesToEmpty.push_back(pTempTile);
                DoHorizontalSearch(iTempIndex, jTempIndex, pTempTile, tempHorizontalTilesToEmpty);
                if(tempHorizontalTilesToEmpty.size() <= 2)
                {
                    tempHorizontalTilesToEmpty.clear();
                }
                for(Tile *pTempTile: tempHorizontalTilesToEmpty)
                {
                    horizontalTilesToEmpty.push_back(pTempTile);
                }
            }
        }
    }
    
    vector<Object *> items;
    
    if(horizontalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: horizontalTilesToEmpty)
        {
            if(std::find(items.begin(), items.end(), pTempTile->GetObject()) == items.end())
            {
                items.push_back(pTempTile->GetObject());
            }
        }
    }
    
    if(verticalTilesToEmpty.size() > 2)
    {
        for(Tile * pTempTile: verticalTilesToEmpty)
        {
            if(std::find(items.begin(), items.end(), pTempTile->GetObject()) == items.end())
            {
                items.push_back(pTempTile->GetObject());
            }
        }
    }
    
    //If magic wand was not used to delete the objects then the move is eligible for a magic wand
    if(!mDoesMagicWandExists &&
       !mpLastMove->mMagicWandUsed &&
       (horizontalTilesToEmpty.size() + verticalTilesToEmpty.size()) >= MATCH_NEEDED_TO_GET_MAGIC_WAND)
    {
        return true;
    }
    
    return false;
}

/*
 Used by the scoreboard to show timer
 */
double GameScreen::GetElapsedSeconds()
{
    time_t currentTime = time(NULL);
    return difftime(currentTime, mStartTime);
}

void GameScreen::ShowEndOfMoveAnimations()
{
    if(mIsBoardLoading)
    {
        return;
    }
    
    //TakeSnapshot();
    queue<shared_ptr<Animation>> mEndOfMoveAnimations;
    
    if(mpLastMove->mEarnedMagicWand)
    {
        int x = (BOARD_WIDTH/2) - (wandImage.mpImage->w/2);
        int y = (BOARD_HEIGHT/2) - (wandImage.mpImage->h/2);
        int w = OBJECT_WIDTH;
        int h = OBJECT_HEIGHT;

        shared_ptr<Image> spWandImage = std::make_shared<Image>(WAND_IMAGE, WAND_IMAGE, Rectangle(Point(x, y), w, h));

        std::shared_ptr<ExplodeAnimation> spExplodeAnimation(spWandImage, 5);
        spExplodeAnimation->OnEnd = WandExplodeAnimationOnEndHandler;        //TOTO: At the end of animation remove the object from the display list or set it active false
        AddObjectToDisplayList(spWandImage);
        AddAnimationToList(spExplodeAnimation);
        mEndOfMoveAnimations.push(spExplodeAnimation);
    }
    
    if(mpLastMove->mItemsCollected >= MATCH_NEEDED_TO_GET_MAGIC_WAND)
    {
//        TextBox greetingsMessage(mGameBoardFontColour, mpPixelFormat);
        string greetingsText;
        greetingsText.append(std::to_string(mpLastMove->mItemsCollected).c_str());

//        int objectWidth = OBJECT_WIDTH;
//        int objectHeight = OBJECT_HEIGHT;
        
        int x = BOARD_WIDTH/4;
        int y = BOARD_HEIGHT/4;
        int w = BOARD_WIDTH/2;
        int h = BOARD_HEIGHT/2;
        shared_ptr<TextBox> spgreetingsMessage = std::make_shared<TextBox>("Greetings Message", greetingsText, GAMEBOARD_FONT_COLOUR_RGBA, Rectangle(Point(x, y), w, h));

        std::shared_ptr<ExplodeAnimation> spExplodeAnimation(spgreetingsMessage, 5);
        spExplodeAnimation->OnEnd = greetingsExplodeAnimationOnEndHandler;
        AddObjectToDisplayList(spgreetingsMessage);
        AddAnimationToList(spExplodeAnimation);
        mEndOfMoveAnimations.push(spExplodeAnimation);

//        greetingsMessage.Update(greetingsText.c_str(), BOARD_HEIGHT/2);
        
//        ExplodeAnimation(&greetingsMessage, 5, mpSnapshot);
//        SDL_Delay(500);
        
//        objectWidth = OBJECT_WIDTH;
//        objectHeight = OBJECT_HEIGHT;
//        greetingsMessage.mPosition.x = (BOARD_WIDTH/2) - (objectWidth/2);
//        greetingsMessage.mPosition.y = (BOARD_HEIGHT/2) - (objectHeight/2);
//        greetingsMessage.mPosition.w = objectWidth;
//        greetingsMessage.mPosition.h = objectHeight;
        
//        mpAudioManager->PlaySoundEffect(MOVE_SOUND_EFFECT);
//        MoveAnimation(&greetingsMessage, &mpBasket->mPosition, MOVE_ANIMATION_SPEED/2, mpSnapshot);
        //TODO: Add move animation
    }
    
//    TakeSnapshot();
    if(mpLastMove->mItemsCollected >= NUMBER_OF_DELETES_FOR_MULTIPLIER)
    {
        //Give Multiplier
        Metrics::GetInstance()->mRelicsEarned++;
        //TODO: Explode and move animations
//        Image relicImage;
//        relicImage.mpImage = mpTextureManager->Get(FTUE_RELIC_IMAGE);
//        relicImage.mPosition.x = (BOARD_WIDTH/2) - (relicImage.mpImage->w/2);
//        relicImage.mPosition.y = (BOARD_HEIGHT/2) - (relicImage.mpImage->h/2);
//        relicImage.mPosition.w = relicImage.mpImage->w;
//        relicImage.mPosition.h = relicImage.mpImage->h;
//
//        ExplodeAnimation(&relicImage, 5, mpSnapshot);
//        SDL_Delay(500);
//        mpAudioManager->PlaySoundEffect(MOVE_SOUND_EFFECT);
//        relicImage.mpImage = mpTextureManager->Get(SPEED_IMAGE);
//        relicImage.mPosition.x = (BOARD_WIDTH/2) - (relicImage.mpImage->w/2);
//        relicImage.mPosition.y = (BOARD_HEIGHT/2) - (relicImage.mpImage->h/2);
//        relicImage.mPosition.w = relicImage.mpImage->w;
//        relicImage.mPosition.h = relicImage.mpImage->h;
//        MoveAnimation(&relicImage, &mpMultiplier->mPosition, MOVE_ANIMATION_SPEED, mpSnapshot);
//        SetScoreBoardValues();
    }
}

void WandExplodeAnimationOnEndHandler()
{
}

void GameScreen::MusicIconOnClickHandler()
{
    AudioManager::GetInstance()->ToggleMusicState();
    if(AudioManager::GetInstance()->MusicState())
    {
        mpMusicIcon->mpImage = TextureManager::GetInstance(mpPixelFormat)->Get(MUSIC_ON_ICON);
    }
    else
    {
        mpMusicIcon->mpImage = TextureManager::GetInstance(mpPixelFormat)->Get(MUSIC_OFF_ICON);
    }
}

void GameScreen::SoundEffectsIconOnClickHandler()
{
    AudioManager::GetInstance()->ToggleSoundEffectsState();
    if(AudioManager::GetInstance()->SoundEffectsState())
    {
        mpSoundEffectsIcon->mpImage = TextureManager::GetInstance(mpPixelFormat)->Get(SOUND_EFFECTS_ON_ICON);
    }
    else
    {
        mpSoundEffectsIcon->mpImage = TextureManager::GetInstance(mpPixelFormat)->Get(SOUND_EFFECTS_OFF_ICON);
    }
}

/*Event Handlers*/
void MouseButtonClickHandler(Point point)
{
    delete mpLastMove;
    mpLastMove = new Move();
    /*Select the object*/
    if(!mpSourceTile)
    {
        if(point.GetX()/TILE_WIDTH < ROWS &&
           point.GetY()/TILE_HEIGHT < COLUMNS)
        {
            mpSourceTile = &mTiles[point.GetX()/TILE_WIDTH][point.GetY()/TILE_HEIGHT];
        }
    }
    else
    {
        if(point.GetX()/TILE_WIDTH < ROWS &&
           point.GetY()/TILE_HEIGHT < COLUMNS)
        {
            mpTargetTile = &mTiles[point.GetX()/TILE_WIDTH][point.GetY()/TILE_HEIGHT];
            mIsSwapRequestedByPlayer = true;
        }
    }
}

void DragHandler(Point startingPoint, Point currentPoint)
{
    delete mpLastMove;
    mpLastMove = new Move();
    if(!mpSourceTile &&
       !mpTargetTile)
    {
        if(startingPoint.GetX()/TILE_WIDTH < ROWS &&
           startingPoint.GetY()/TILE_HEIGHT < COLUMNS &&
           currentPoint.GetX()/TILE_WIDTH < ROWS &&
           currentPoint.GetY()/TILE_HEIGHT < COLUMNS)
        {
            Tile * pSourceTile = &mTiles[startingPoint.GetX()/TILE_WIDTH][startingPoint.GetY()/TILE_HEIGHT];
            Tile * pTargetTile = &mTiles[currentPoint.GetX()/TILE_WIDTH][currentPoint.GetY()/TILE_HEIGHT];
            if(pSourceTile != pTargetTile)
            {
                mpSourceTile = pSourceTile;
                mpTargetTile = pTargetTile;
                mIsSwapRequestedByPlayer = true;
            }
        }
    }
}

void DropHandler(Point startingPoint, Point currentPoint)
{
    //    mpSourceTile = NULL;
    //    mpTargetTile = NULL;
}

void OnUnLoad()
{
    
}

GameScreen::~GameScreen()
{
    /*Delete objects used in the game*/
    for(int i=0;i<ROWS;i++)
    {
        for(int j=0;j<COLUMNS;j++)
        {
            delete mTiles[i][j].GetObject();
        }
    }

    mpAudioManager->PauseMusic();
    
    /*Delete score board related objects*/
    delete mpBasket;
    delete mpCollectedCount;
    delete mpMultiplier;
    delete mpMultiplierValue;
    delete mpScore;
    delete mpScoreValue;
    delete mpMusicIcon;
    delete mpSoundEffectsIcon;
    delete mpTimer;
    delete mpTimerValue;
    delete mpLastMove;
    
    SDL_FreeSurface(mpSnapshot);
    for(string * pItemName:mItemNames)
    {
        delete pItemName;
    }
    mItemNames.clear();
}
