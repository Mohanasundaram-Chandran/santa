//
//  GameOverScreen.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef GameOverScreen_h
#define GameOverScreen_h

#include <SDL2/SDL.h>
#include "Screen.h"
#include "GestureReader.h"
#include "TextBox.h"
#include "Global.h"
#include "AudioManager.h"
#include "TextureManager.h"
#include "TextBox.h"
#include "Metrics.h"

class GameOverScreen: public Screen
{
private:
    static bool mQuit;
    
    /*Scoreboard related*/
    std::shared_ptr<Image> mspBasket;
    std::shared_ptr<TextBox> mspCollectedCount;
    std::shared_ptr<TextBox> mspMultiplicationSymbol;
    std::shared_ptr<Image> mspMultiplier;
    std::shared_ptr<TextBox> mspMultiplierValue;
    std::shared_ptr<TextBox> mspEqualToSymbol;
    std::shared_ptr<Image> mspScore;
    std::shared_ptr<TextBox> mspScoreValue;
    std::shared_ptr<Image> mspMusicIcon;
    std::shared_ptr<Image> mspSoundEffectsIcon;
    std::shared_ptr<Image> mspScreenBackground;
    
    //Event handlers
    static void MusicIconOnClickHandler(Point clickedPoint);
    static void SoundEffectsIconOnClickHandler(Point clickedPoint);
    static void GameOverTextOnClickHandler(Point clickedPoint);
    
public:
    GameOverScreen();
    void BeforeDrawingEveryFrame() override;
};

#endif /* GameOverScreen_h */
