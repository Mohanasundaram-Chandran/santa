//
//  Tile.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//
// This class represents a tile which makes up the grid.
// There are 8X8 = 64 tiles in the grid.

#include "Image.h"

#ifndef Tile_h
#define Tile_h

class Tile
{
private:
    std::shared_ptr<Image> mspImage;
    Rectangle mRectangle;
    
public:
    void SetPosition(Point point) noexcept;
    void DropObject(std::shared_ptr<Image> spImage) noexcept;
    void DeleteObject() noexcept;
    std::shared_ptr<Image> PickUpObject() const, noexcept;
    std::shared_ptr<Image> GetObject() const, noexcept;
};

#endif /* Tile_h */
