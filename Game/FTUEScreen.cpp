//
//  FTUEScreen.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 21/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "../Engine/Kernel/Engine.h"//For YIELD
#include "../Engine/SDL/Rectangle.h"

#include "FTUEScreen.h"
#include "Global.h"

FTUEScreen::FTUEScreen()
{
    /*Preload sounds for runtime performance*/
    //These two functions should be wrapped around Load and Play in Screen base class
    Engine::GetAudioManager()->LoadMusic(BACKGROUND_MUSIC);

    mspFtueImage1 = std::make_shared<Image>(FTUE_IMAGE_1, FTUE_IMAGE_1, Rectangle(Point(0,0), SCREEN_WIDTH, SCREEN_HEIGHT));
    mspFtueImage2 = std::make_shared<Image>(FTUE_IMAGE_2, FTUE_IMAGE_2, Rectangle(Point(0,0), SCREEN_WIDTH, SCREEN_HEIGHT));
    mspFtueImage3 = std::make_shared<Image>(FTUE_IMAGE_3, FTUE_IMAGE_3, Rectangle(Point(0,0), SCREEN_WIDTH, SCREEN_HEIGHT));
    mspFtueImage4 = std::make_shared<Image>(FTUE_IMAGE_4, FTUE_IMAGE_4, Rectangle(Point(0,0), SCREEN_WIDTH, SCREEN_HEIGHT));
    mspFtueImage5 = std::make_shared<Image>(FTUE_IMAGE_5, FTUE_IMAGE_5, Rectangle(Point(0,0), SCREEN_WIDTH, SCREEN_HEIGHT));
    mspFtueWandImage = std::make_shared<Image>(FTUE_WAND_IMAGE, FTUE_WAND_IMAGE, Rectangle(Point(0,0), 256, 256));
    mspFtueRelicImage = std::make_shared<Image>(FTUE_RELIC_IMAGE, FTUE_RELIC_IMAGE, Rectangle(Point(0,0), 220, 220));
    
    mspFtueImage1->SetActive(false);
    mspFtueImage2->SetActive(false);
    mspFtueImage3->SetActive(false);
    mspFtueImage4->SetActive(false);
    mspFtueImage5->SetActive(false);
    mspFtueWandImage->SetActive(false);
    mspFtueRelicImage->SetActive(false);
    
    this->AddObjectToDisplayList(mspFtueImage1);
    this->AddObjectToDisplayList(mspFtueImage2);
    this->AddObjectToDisplayList(mspFtueImage3);
    this->AddObjectToDisplayList(mspFtueImage4);
    this->AddObjectToDisplayList(mspFtueImage5);
    this->AddObjectToDisplayList(mspFtueWandImage);
    this->AddObjectToDisplayList(mspFtueRelicImage);
    
    Colour textColour FTUE_FONT_COLOUR_RGBA;
    Colour textBackgroundColour FTUE_FONT_BACKGROUND_COLOUR_RGBA;

    mspInfoText = std::make_shared<TextBox>(FTUE_INFO_TEXT_OBJECT_NAME, FTUE_INFO_TEXT1, textColour, textBackgroundColour, Rectangle(Point(0,0), BOARD_WIDTH, OBJECT_HEIGHT));
    this->AddObjectToDisplayList(mspInfoText);
    
    int x = mspInfoText->GetRectangle().GetWidth()/2;
    int y = mspInfoText->GetRectangle().GetHeight()/2;
    mspInfoTextExplodeAnimation = std::make_shared<ExplodeAnimation>(mspInfoText, Point(x, y), FTUE_EXPLODE_ANIMATION_SPEED);
    this->AddAnimationToList(mspInfoTextExplodeAnimation);

    mspMagicWandExplodeAnimation = std::make_shared<ExplodeAnimation>(mspFtueWandImage, Point(SCREEN_WIDTH/2, SCREEN_HEIGHT/2), FTUE_EXPLODE_ANIMATION_SPEED);
    this->AddAnimationToList(mspMagicWandExplodeAnimation);
    mspRelicExplodeAnimation = std::make_shared<ExplodeAnimation>(mspFtueRelicImage, Point(SCREEN_WIDTH/2, SCREEN_HEIGHT/2), FTUE_EXPLODE_ANIMATION_SPEED);
    this->AddAnimationToList(mspRelicExplodeAnimation);
}

void FTUEScreen::OnLoad()
{
    mShouldEndScreenAtTheEndOfThisFrame = false;
    mIsFTUECompleted = false;
    Engine::GetAudioManager()->PlayMusic(BACKGROUND_MUSIC, AudioManager::MaxVolume);

    mspInfoTextExplodeAnimation->mOnEnd = std::bind(&FTUEScreen::OnExplodeAnimationEndHandler, this);
    mspMagicWandExplodeAnimation->mOnEnd = std::bind(&FTUEScreen::OnExplodeAnimationEndHandler, this);
    mspRelicExplodeAnimation->mOnEnd = std::bind(&FTUEScreen::OnExplodeAnimationEndHandler, this);
    mOnMouseButtonClick = std::bind(&FTUEScreen::OnMouseButtonClickHandler, this, std::placeholders::_1);;
    
    mspFtueImage1->SetZIndex(0);
    mspFtueImage1->SetActive(true);
    mspInfoText->SetZIndex(1);
    mspInfoText->SetActive(true);
    mspInfoTextExplodeAnimation->SetActive(true);
    mspInfoTextExplodeAnimation->Start();
}

void FTUEScreen::OnExplodeAnimationEndHandler()
{
    /*Implements a story board.*/
    mStep++;
    switch (mStep)
    {
        case 0:
            break;
        case 1:
            mspInfoText->Set(FTUE_INFO_TEXT2);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 2:
            mspFtueImage1->SetActive(false);
            mspFtueImage2->SetActive(true);
            mspInfoText->Set(FTUE_INFO_TEXT3);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 3:
            mspFtueImage2->SetActive(false);
            mspFtueImage4->SetActive(true);
            mspInfoText->Set(FTUE_INFO_TEXT4);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 4:
            mspInfoText->Set(FTUE_INFO_TEXT5);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 5:
            mspFtueImage4->SetActive(false);
            mspFtueImage5->SetActive(true);
            mspInfoText->Set(FTUE_INFO_TEXT6);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 6:
            mspInfoText->Set(FTUE_INFO_TEXT7);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 7:
            mspFtueWandImage->SetActive(true);
            mspMagicWandExplodeAnimation->SetActive(true);
            mspMagicWandExplodeAnimation->Start();
            break;
        case 8:
            mspInfoText->Set(FTUE_INFO_TEXT8);
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 9:
            mspInfoText->Set("All instances of that item will be deleted");
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 10:
            mspFtueWandImage->SetActive(false);
            mspInfoText->Set("In 1 move delete 20 objects");
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 11:
            mspInfoText->Set("You get a speed relic");
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 12:
            mspFtueRelicImage->SetActive(true);
            mspRelicExplodeAnimation->SetActive(true);
            mspRelicExplodeAnimation->Start();
            break;
        case 13:
            mspInfoText->Set("It multiplies your score");
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 14:
            mspFtueRelicImage->SetActive(false);
            mspInfoText->Set("You have one minutue. Go!");
            mspInfoTextExplodeAnimation->SetActive(true);
            mspInfoTextExplodeAnimation->Start();
            break;
        case 15:
            mIsFTUECompleted = true;
            break;
    }
}

void FTUEScreen::OnMouseButtonClickHandler(Point point)
{
    if(mIsFTUECompleted)
    {
        mShouldEndScreenAtTheEndOfThisFrame = true;
    }
}

void FTUEScreen::BeforeDrawingEveryFrame()
{
//    /*Implements a story board.*/
//    switch (mStep)
//    {
//        case 0:
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//        case 1:
//            YIELD(2000);//After the animation is over wait for 2 seconds
//            mspFtueImage1->SetActive(false);
//            mspFtueImage2->SetActive(true);
//            mspInfoText->Set(FTUE_INFO_TEXT2);
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 2:
//            YIELD(2000);
//            mspFtueImage2->SetActive(false);
//            mspFtueImage4->SetActive(true);
//            mspInfoText->Set(FTUE_INFO_TEXT3);
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 3:
//            YIELD(2000);
//            mspInfoText->Set(FTUE_INFO_TEXT4);
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 4:
//            YIELD(2000);
//            mspFtueImage4->SetActive(false);
//            mspFtueImage5->SetActive(true);
//            mspInfoText->Set("If you match 5 items");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 5:
//            YIELD(2000);
//            mspInfoText->Set("You get a bonus magic wand");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 6:
//            YIELD(2000);
//            mspWandImage->SetActive(true);
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 7:
//            YIELD(2000);
//            mspInfoText->Set("Swap the wand with an adjacent item");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 8:
//            YIELD(2000);
//            mspInfoText->Set("All instances of that item will be deleted");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 9:
//            YIELD(2000);
//            mspInfoText->Set("In 1 move delete 20 objects");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 10:
//            YIELD(2000);
//            mspInfoText->Set("You get a speed relic");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 11:
//            YIELD(2000);
//            mspFtueRelicImage->SetActive(true);
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 12:
//            YIELD(2000);
//            mspInfoText->Set("It multiplies your score");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 13:
//            YIELD(2000);
//            mspInfoText->Set("You have one minutue. Go!");
//            mspExplodeAnimation->SetActive(true);
//            if(mspExplodeAnimation->IsActive())
//            {
//                return;//Wait till the animation gets over
//            }
//            mStep++;
//        case 14:
//            YIELD(2000);
//    }
}

void FTUEScreen::OnUnload()
{
    Engine::GetAudioManager()->PauseMusic();
}

FTUEScreen::~FTUEScreen()
{
}
