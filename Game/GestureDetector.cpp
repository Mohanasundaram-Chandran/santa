//
//  GestureDetector.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 13/08/17.
//  Copyright © 2017 Cube. All rights reserved.
//

#include "GestureDetector.h"

GestureDetector::GestureDetector()
{
    mDummy.mGestureName = Gesture::GestureName::None;
    mDummy.mStartingX = 0;
    mDummy.mStartingY = 0;
    mDummy.mCurrentX = 0;
    mDummy.mCurrentY = 0;
}

GestureDetector::Gesture & GestureDetector::Input(Event event)
{
    switch(event)
    {
        case Event::MouseButtonDown:
            mGesture.mAction = Gesture::Action::None;
            SDL_GetMouseState(&mGesture.mStartingX, &mGesture.mStartingY);
            mGesture.mCurrentX = mGesture.mStartingX;
            mGesture.mCurrentY = mGesture.mStartingY;
            mPreviousActions.clear();
            mPreviousActions.push_back(e.type);
            return mGesture;
            break;
        case Event::MouseMove:
            if(mPreviousActions.size() > 0 &&
               SDL_MOUSEBUTTONDOWN == mPreviousActions[0])
            {
                mGesture.mAction = Gesture::Action::Drag;
                SDL_GetMouseState(&mGesture.mCurrentX, &mGesture.mCurrentY);
                if(mPreviousActions.size() < 2)
                {
                    mPreviousActions.push_back(e.type);
                }
                return mGesture;
            }
            break;
        case Event::MouseButtonUp:
            if(mPreviousActions.size() == 1)
            {
                mGesture.mAction = Gesture::Action::Click;
            }
            if(mPreviousActions.size() == 2)
            {
                mGesture.mAction = Gesture::Action::Drop;
            }
            SDL_GetMouseState(&mGesture.mCurrentX, &mGesture.mCurrentY);
            mPreviousActions.clear();
            return mGesture;
            break;
    }
    return mDummy;
}
