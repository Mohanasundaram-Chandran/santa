//
//  Tile.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "Tile.h"

void Tile::SetPosition(Point point) noexcept
{
    mRectangle.SetPosition(point, TILE_WIDTH, TILE_HEIGHT);
}

void Tile::DropObject(std::shared_ptr<Image> spImage) noexcept
{
    mspImage = spImage;
    if(mspImage)
    {
        mspImage->SetRectangle(mRectangle);
        mspImage->SetTile(this);
    }
}

void Tile::DeleteObject() noexcept
{
    mspImage.reset();
}

std::shared_ptr<Image> Tile::PickUpObject() const, noexcept
{
    return mspImage;
}

std::shared_ptr<Image> Tile::GetObject() const, noexcept
{
    return mspImage;
}
