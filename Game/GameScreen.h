//
//  GameScreen.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//
//  This class represents the game board.
//  It contains 8X8 = 64 Tiles and each tile contains a coloured object.

#ifndef GameScreen_h
#define GameScreen_h

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <iostream>
#include <vector>
#include <ctime>
using namespace std;

#include "Global.h"
#include "Item.h"
#include "Tile.h"
#include "GestureReader.h"
#include "TextBox.h"
#include "AudioManager.h"
#include "TextureManager.h"
#include "Screen.h"

class GameScreen: public Screen
{
private:
    class Move
    {
    public:
        int mItemsMatched = 0;
        int mItemsCollected = 0;
        bool mMagicWandUsed = false;
        bool mEarnedMagicWand = false;
        bool mEarnedReindeerRelic = false;
    }*mpLastMove;
    
    vector<string> mItemNames;//Contains the list of item names
    Tile mTiles[ROWS][COLUMNS];//8X8 tiles which make up the grid of the game board
    Tile * mpSourceTile = NULL;//When a swap happens this variable holds the source Tile
    Tile * mpTargetTile = NULL;//When a swap happens this variable holds the target Tile
    bool mIsBoardLoading = false;
    Rectangle mRectangle(Point(0,0), BOARD_WIDTH, BOARD_HEIGHT);

    SDL_Colour mGameBoardFontColour = GAMEBOARD_FONT_COLOUR;
    SDL_Colour mScoreBoardFontColour = SCOREBOARD_FONT_COLOUR;

    /*Scoreboard related*/
    std::shared_ptr<Image>mspScoreBoardBackground;
    std::shared_ptr<Image> mspBasket;
    std::shared_ptr<TextBox> mspCollectedCount;
    std::shared_ptr<TextBox> mspMultiplicationSymbol;
    std::shared_ptr<Image> mspMultiplier;
    std::shared_ptr<TextBox> mspMultiplierValue;
    std::shared_ptr<TextBox> mspEqualToSymbol;
    std::shared_ptr<Image> mspScore;
    std::shared_ptr<TextBox> mspScoreValue;
    std::shared_ptr<Image> mspMusicIcon;
    std::shared_ptr<Image> mspSoundEffectsIcon;
    std::shared_ptr<Image> mspTimer;
    std::shared_ptr<TextBox> mspTimerValue;
    std::shared_ptr<TextBox> mspLoadingText;
    std::shared_ptr<TextBox> mspGoText;
    
    time_t mStartTime;//Game start time
    
    Tile * mpLastTileEmptied = NULL;
    
    bool mDoesMagicWandExists = false;

    void SwapObjects(Tile *pSourceTile, Tile *pTargetTile, bool shouldSwapAnimate);
    bool DeleteMatchingObjects(Tile * pTile);
    bool DeleteMatchingObjectsAndCollapseRecursively();
    bool IsAdjacentSwap();
    bool PullDown(Tile *pTile);
    void Collapse();
    bool DoMagic();
    void DoHorizontalSearch(int iIndex, int jIndex, Tile * pTile, vector<Tile *> &horizontalTilesToEmpty);
    void DoVerticalSearch(int iIndex, int jIndex, Tile * pTile, vector<Tile *> &verticalTilesToEmpty);
    double GetElapsedSeconds();
    void HandleMagicWand(Tile *pSourceTile, Tile *pTargetTile);
    
    void SetScoreBoardValues();
//    void DrawBoard(int delay);
    
    //Create a generic animation class if time permits. This implementation is lame
    void DeleteAnimation(Item * pItem, int moveSpeed, int pulseSpeed);
    void ShowEndOfMoveAnimations();
 
    AudioManager *mpAudioManager = NULL;
    TextureManager *mpTextureManager = NULL;
    bool mIsMatchSoundEffectPlayed = false;
    
    //Event handlers
    static void MusicIconOnClickHandler();
    static void SoundEffectsIconOnClickHandler();

    bool IsEligibleForMagicWand(Tile * pTile);
    
    /*Just to play the animation in the sequence created*/
    std::vector<std::shared_ptr<Animation>> mspObjectAnimations;
    std::vector<std::shared_ptr<object>> mspObjects;
    
    bool mIsSwapRequestedByPlayer = false;//Tells the BeforeDrawingEveryFrame to process the algorithm as there was valid input
    bool mIsSwapInProgress = false;
    
public:
    class UnableToLoadSoundException{};
    GameScreen(SDL_Window * pWindow, SDL_PixelFormat *pPixelFormat, SDL_Rect position);
    ~GameScreen();
};

#endif /* GameScreen_h */
