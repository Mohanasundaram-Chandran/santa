//
//  GameOverScreen.cpp
//  Cube
//
//  Created by Mohanasundaram Chandran on 11/12/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#include "GameOverScreen.h"

GameOverScreen::GameOverScreen()
{
    /*Scoreboard objects*/
    /*First Row*/
    mspBasket = std::make_shared<Image>(COLLECTED_IMAGE, COLLECTED_IMAGE, Rectangle(Point(OBJECT_WIDTH * 0,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplier = std::make_shared<Image>(SPEED_IMAGE, SPEED_IMAGE, Rectangle(Point(OBJECT_WIDTH * 2,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspScore = std::make_shared<Image>(SCORE_IMAGE, SCORE_IMAGE, Rectangle(Point(OBJECT_WIDTH * 4,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));

    if(AudioManager::GetInstance()->MusicState())
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_ON_ICON, MUSIC_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    else
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_OFF_ICON, MUSIC_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    mpMusicIcon->mOnClick = MusicIconOnClickHandler;
    
    /*Second Row*/
    mspCollectedCount = std::make_shared<TextBox>("Collected Count", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 0,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplicationSymbol = std::make_shared<TextBox>("Multiplication Symbol", "x", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 1,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspMultiplierValue = std::make_shared<TextBox>("Multiplier Value", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 2,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspEqualToSymbol = std::make_shared<TextBox>("Equal To Symbol", "=", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 3,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));
    mspScoreValue = std::make_shared<TextBox>("Score Value", "0", SCOREBOARD_FONT_COLOUR_RGBA,Rectangle(Point(OBJECT_WIDTH * 4,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2)), OBJECT_WIDTH, OBJECT_HEIGHT));

    if(AudioManager::GetInstance()->SoundEffectsState())
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_ON_ICON, SOUND_EFFECTS_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2), OBJECT_WIDTH, OBJECT_HEIGHT)));
    }
    else
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_OFF_ICON, SOUND_EFFECTS_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2), OBJECT_WIDTH, OBJECT_HEIGHT)));
    }
    mpSoundEffectsIcon->mOnClick = SoundEffectsIconOnClickHandler;
    
    Colour boardBackgroundColour(BOARD_BACKGROUND_COLOUR_RGB);
    mspGameOver = std::make_shared<TextBox>("Game Over", "0", GAMEBOARD_FONT_COLOUR, boardBackgroundColour, Rectangle(Point(0, 0), BOARD_WIDTH, BOARD_HEIGHT));
    mspGameOver->mOnClick = GameOverTextOnClickHandler;
    
    mspScreenBackground = std::make_shared<Image>(SOUND_EFFECTS_OFF_ICON, boardBackgroundColour, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2), OBJECT_WIDTH, OBJECT_HEIGHT)));
    mspScreenBackground->mZIndex--;
}

void GameOverScreen::BeforeDrawingEveryFrame()
{
    /* Draw Score Board*/
    mspCollectedCount->Set(std::to_string(Metrics::GetInstance()->mItemsCollected));
    string multiplierText;
    multiplierText.append(std::to_string(Metrics::GetInstance()->mRelicsEarned));
    mspMultiplierValue->Set(multiplierText);
    mspScoreValue->Set(std::to_string(Metrics::GetInstance()->mItemsCollected * Metrics::GetInstance()->mRelicsEarned));

}

void GameOverScreen::MusicIconOnClickHandler(Point clickedPoint)
{
    AudioManager::GetInstance()->ToggleMusicState();
    if(AudioManager::GetInstance()->MusicState())
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_ON_ICON, MUSIC_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
    else
    {
        mspMusicIcon = std::make_shared<Image>(MUSIC_OFF_ICON, MUSIC_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - SCORE_BOARD_HEIGHT), OBJECT_WIDTH, OBJECT_HEIGHT));
    }
}

void GameOverScreen::SoundEffectsIconOnClickHandler(Point clickedPoint)
{
    AudioManager::GetInstance()->ToggleSoundEffectsState();
    if(AudioManager::GetInstance()->SoundEffectsState())
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_ON_ICON, SOUND_EFFECTS_ON_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2), OBJECT_WIDTH, OBJECT_HEIGHT)));
    }
    else
    {
        mspSoundEffectsIcon = std::make_shared<Image>(SOUND_EFFECTS_OFF_ICON, SOUND_EFFECTS_OFF_ICON, Rectangle(Point(OBJECT_WIDTH * 6,SCREEN_HEIGHT - (SCORE_BOARD_HEIGHT / 2), OBJECT_WIDTH, OBJECT_HEIGHT)));
    }
}

void GameOverScreen::GameOverTextOnClickHandler(Point clickedPoint)
{
    mShouldEndScreenAtTheEndOfThisFrame = true;
}
