//
//  FTUEScreen.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 21/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef FTUEScreen_h
#define FTUEScreen_h

#include <vector>
#include <memory>
using namespace std;

#include "../Engine/UI/Screen.h"
#include "../Engine/UI/Image.h"
#include "../Engine/UI/Animation.h"
#include "../Engine/UI/TextBox.h"
#include "Animations/ExplodeAnimation.h"

class FTUEScreen: public Screen
{
private:
    shared_ptr<Image> mspFtueImage1;
    shared_ptr<Image> mspFtueImage2;
    shared_ptr<Image> mspFtueImage3;
    shared_ptr<Image> mspFtueImage4;
    shared_ptr<Image> mspFtueImage5;
    shared_ptr<Image> mspFtueWandImage;
    shared_ptr<Image> mspFtueRelicImage;

    std::shared_ptr<TextBox> mspInfoText;    
    
    shared_ptr<Animation> mspInfoTextExplodeAnimation;
    shared_ptr<Animation> mspMagicWandExplodeAnimation;
    shared_ptr<Animation> mspRelicExplodeAnimation;
    
    size_t mStep = 0;
    bool mIsFTUECompleted = false;

public:
    FTUEScreen();
    void BeforeDrawingEveryFrame() override;
    void OnLoad() override;
    void OnUnload() override;
    void OnExplodeAnimationEndHandler();
    void OnMouseButtonClickHandler(Point point);
    ~FTUEScreen();
};
#endif /* FTUEScreen_h */
