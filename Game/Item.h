//
//  Item.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 12/10/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//
// This class represents an Item
//

#ifndef Item_h
#define Item_h

#include <SDL2/SDL.h>
#include "Global.h"
#include "Image.h"

class Tile;

class Item
{
    std::shared_ptr<Image> mspObject;
    Tile * mpTile = NULL;//Always associated with a tile. Practically will never be NULL. TODO: Later to get rid of this
public:
    SetObject(std::shared_ptr<Image> spObject);
    SetTile(Tile * pTile);
    Item();
};

#endif /* Item_h */
