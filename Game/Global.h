//
//  Global.h
//  Cube
//
//  Created by Mohanasundaram Chandran on 01/11/16.
//  Copyright © 2016 Mohanasundaram Chandran. All rights reserved.
//

#ifndef Global_h
#define Global_h

/*Game related*/
#define LOADING_SCREEN_IN_SECONDS 15
#define GAME_TIME_IN_SECONDS 60
#define FRAMES_PER_SECOND 60
#define NUMBER_OF_DELETES_FOR_MULTIPLIER 20
#define MATCH_NEEDED_TO_GET_MAGIC_WAND 5

/*Board related*/
#define OBJECT_WIDTH 60
#define OBJECT_HEIGHT 60

#define ROWS 8
#define COLUMNS 8

#define BOARD_WIDTH (OBJECT_WIDTH * ROWS)
#define BOARD_HEIGHT (OBJECT_HEIGHT * COLUMNS)

#define SCORE_BOARD_WIDTH BOARD_WIDTH
#define SCORE_BOARD_HEIGHT (OBJECT_HEIGHT * 2)

#define SCREEN_WIDTH BOARD_WIDTH
#define SCREEN_HEIGHT (BOARD_HEIGHT + SCORE_BOARD_HEIGHT)

#define TILE_WIDTH OBJECT_WIDTH
#define TILE_HEIGHT OBJECT_HEIGHT

#define FONT_SIZE 54

/*Animation speeds*/
#define ITEM_MOVE_ANIMATION_SPEED 40 //Directly propotional
#define MOVE_ANIMATION_SPEED 20 //Directly propotional
#define PULSE_ANIMATION_SPEED 5 //Directly propotional
#define MULTIPLIER_ANIMATION_SPEED 5 //Directly propotional
#define PULL_DOWN_ANIMATION_SPEED 1 //Inversly propotional
#define SWAP_ANIMATION_SPEED 5 //Inversely propotional
#define SWAP_ANIMATION_LOOP_INCREMENT 7 //Directly propotional
#define IMPLODE_ANIMATION_SPEED 5
#define EXPLODE_ANIMATION_SPEED 5
#define FTUE_EXPLODE_ANIMATION_SPEED 5

/*Colours*/
#define PASTAL_BROWN 0xF39B97
#define PASTAL_BROWN_RGB {243, 155, 151}

#define PASTAL_BLUE 0x72B6AB
#define PASTAL_BLUE_RGB {114, 182, 171}

#define PASTAL_ORANGE 0xFF9B67
#define RED 0XF83732

#define BOARD_BACKGROUND_COLOUR 0XD5EAC3
#define BOARD_BACKGROUND_COLOUR_RGBA (213, 234, 195, 0)
#define SCOREBOARD_BACKGROUND_COLOUR 0X676A3E
#define SCOREBOARD_BACKGROUND_COLOUR_RGBA (103, 106, 62, 0)

#define FTUE_FONT_COLOUR_RGBA (0, 0, 0, 0)
#define FTUE_FONT_BACKGROUND_COLOUR_RGBA SCOREBOARD_BACKGROUND_COLOUR_RGBA
#define GAMEBOARD_FONT_COLOUR_RGBA SCOREBOARD_BACKGROUND_COLOUR_RGBA
#define SCOREBOARD_FONT_COLOUR_RGBA Colour(0, 0, 0, 0)

#define GAME_NAME "Santa"//Put all the images in the proper folder structure and rename this game name for reskinning
#define IMAGES_PATH "Media/Images/" GAME_NAME "/"
#define AUDIO_PATH "Media/Audio/" GAME_NAME "/"

#define FTUE_INFO_TEXT_OBJECT_NAME          "Info Text"
#define FTUE_INFO_TEXT1         "This is your game board"
#define FTUE_INFO_TEXT2         "Swap 2 items to make a match"
#define FTUE_INFO_TEXT3         "Like this"
#define FTUE_INFO_TEXT4         "Now 3 items are matched"
#define FTUE_INFO_TEXT5         "You will get 3 points"
#define FTUE_INFO_TEXT6         "If you match 5 items"
#define FTUE_INFO_TEXT7         "You get a bonus magic wand"
#define FTUE_INFO_TEXT8         "Swap the wand with an adjacent item"

#define FTUE_IMAGE_1            IMAGES_PATH "FTUE1.bmp"
#define FTUE_IMAGE_2            IMAGES_PATH "FTUE2.bmp"
#define FTUE_IMAGE_3            IMAGES_PATH "FTUE3.bmp"
#define FTUE_IMAGE_4            IMAGES_PATH "FTUE4.bmp"
#define FTUE_IMAGE_5            IMAGES_PATH "FTUE5.bmp"
#define FTUE_WAND_IMAGE         IMAGES_PATH "FTUEWandIcon.bmp"
#define FTUE_RELIC_IMAGE        IMAGES_PATH "FTUERelicIcon.bmp"//Scaling at run time does not render smoothly. Hence duplicate sizes.

#define BACKGROUND_MUSIC        AUDIO_PATH "Music.mp3"
#define MATCH_SOUND_EFFECT      AUDIO_PATH "Match.wav"
#define DROP_SOUND_EFFECT       AUDIO_PATH "Drop.wav"
#define LIGHTNING_SOUND_EFFECT  AUDIO_PATH "Lightning.wav"
#define POP_SOUND_EFFECT        AUDIO_PATH "Pop.wav"
#define MOVE_SOUND_EFFECT       AUDIO_PATH "Move.wav"

#define WAND_NAME           "Wand.bmp"
#define OBJECT1_NAME        "Object1.bmp"
#define OBJECT2_NAME        "Object2.bmp"
#define OBJECT3_NAME        "Object3.bmp"
#define OBJECT4_NAME        "Object4.bmp"
#define OBJECT5_NAME        "Object5.bmp"

#define WAND_IMAGE              IMAGES_PATH "Wand.bmp"
#define COLLECTED_IMAGE         IMAGES_PATH "Collected.bmp"
#define SPEED_IMAGE             IMAGES_PATH "Speed.bmp"
#define SCORE_IMAGE             IMAGES_PATH "Score.bmp"
#define TIMER_IMAGE             IMAGES_PATH "Timer.bmp"
#define MUSIC_ON_ICON           IMAGES_PATH "MusicOn.bmp"
#define MUSIC_OFF_ICON          IMAGES_PATH "MusicOff.bmp"
#define SOUND_EFFECTS_ON_ICON   IMAGES_PATH "SoundEffectsOn.bmp"
#define SOUND_EFFECTS_OFF_ICON  IMAGES_PATH "SoundEffectsOff.bmp"
#define OBJECT1                 IMAGES_PATH "Object1.bmp"
#define OBJECT2                 IMAGES_PATH "Object2.bmp"
#define OBJECT3                 IMAGES_PATH "Object3.bmp"
#define OBJECT4                 IMAGES_PATH "Object4.bmp"
#define OBJECT5                 IMAGES_PATH "Object5.bmp"

#endif /* Global_h */
